package cl.circle.localesconectados.enviosii;
/**
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;
import java.util.Properties;

import cl.circle.localesconectados.imp.GenerarEnvioDocumentos;
import cl.circle.localesconectados.imp.ConsultaEstadoDocumentos;
import cl.circle.localesconectados.service.*;


public class EnvioSII implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Properties properties = new Properties();
	final private static String PROPERTIES_FILE= "app.prop";
	private static String dataBase; 
	private static String usuario; 
	private static String password; 
	private static String ip; 
	private static String dir;
	private static boolean log = false; 
	private static String appName;
	private static String clientId;
	private static String secret;
	private static String token; 
	private static String refresh; 
	private static String email; 
	private static String certificadoRuta; 
	private static String certificadoPass; 
	private static String wsSeed; 
	private static String wsToken;
	private static String wsEnvio;
	

	public EnvioSII () {
		try {
		    properties.load(new BufferedReader(new FileReader(PROPERTIES_FILE)));
		     
		    if (properties.isEmpty()) {
		    	System.err.println("no existe archivo de propiedades.");
		    	//return; 
		    }
		    dataBase 		= properties.getProperty("dataBase", "CIRCLE_LC_MIGRA");
		    usuario  		= properties.getProperty("user", "circle");
		    password 		= properties.getProperty("password", "cuRY$rSb6aw8");
		    ip		 		= properties.getProperty("ip", "54.245.137.107");
		    dir      		= properties.getProperty("dir", ".");
		    appName  		= properties.getProperty("appName");
		    clientId 		= properties.getProperty("clientId");
		    secret   		= properties.getProperty("secret");
		    token    		= properties.getProperty("token");
		    refresh  		= properties.getProperty("refresh");
		    email    		= properties.getProperty("email", "no-responder@localesconectados.cl");
		    certificadoRuta = properties.getProperty("certificadoRuta");
		    certificadoPass = properties.getProperty("certificadoPass");
		    wsSeed			= properties.getProperty("wsSeed","https://maullin.sii.cl/DTEWS/CrSeed.jws?WSDL");
		    wsToken			= properties.getProperty("wsToken","https://maullin.sii.cl/DTEWS/GetTokenFromSeed.jws?WSDL");
		    wsEnvio			= properties.getProperty("wsEnvio","https://ws2.sii.cl/WSCOMPROBANTEBOLETAAUTCERT/comprobanteboletaservice?wsdl");
		    
		    
		    String tempLog = properties.getProperty("log", "false"); 
		    
		    if(tempLog.contains("true"))
		    log = true; 
		    
		    
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @return the dataBase
	 */
	public static String getDataBase() {
		return dataBase;
	}

	/**
	 * @return the usuario
	 */
	public static String getUsuario() {
		return usuario;
	}

	/**
	 * @return the password
	 */
	public static String getPassword() {
		return password;
	}

    /**
	 * @return the ip
	 */
	public static String getIp() {
		return ip;
	}

	/**
	 * @return the dir
	 */
	public static String getDir() {
		return dir;
	}

	/**
	 * @return the log
	 */
	public static boolean isLog() {
		return log;
	}
	
	public static String getAppName() {
		return appName;
	}

	public static String getClientId() {
		return clientId;
	}

	public static String getSecret() {
		return secret;
	}

	public static String getToken() {
		return token;
	}

	public static String getRefresh() {
		return refresh;
	}

	public static String getEmail() {
		return email;
	}

	public static String getCertificadoRuta() {
		return certificadoRuta;
	}

	public static String getCertificadoPass() {
		return certificadoPass;
	}

	public static String getWsSeed() {
		return wsSeed;
	}

	public static String getWsToken() {
		return wsToken;
	}


	public static String getWsEnvio() {
		return wsEnvio;
	}


	private void generarEnvio () throws Exception {
			GenerarEnvioDocumentos ged= new GenerarEnvioDocumentos(); 
			//ged.generar();
			ged.run();
	}
	
	private void validaEnvio() throws Exception {
		ConsultaEstadoDocumentos val = new ConsultaEstadoDocumentos();
		//val.consulta();
		val.run();
	}

	public static void main(String args[]) throws Exception {
		try {
		EnvioSII rp = new EnvioSII(); 
		if (args.length == 0) {
			System.err.println("no se encotraron los argumentos"); 
		}
		else if (args[0].equals("ENVIAR_SII")) {
			rp.generarEnvio(); 
		}
		else if (args[0].equals("CONSULTA_ESTADO")) {
			rp.validaEnvio(); 
		}
	}
	catch(Exception e) {
		e.printStackTrace();
			System.exit(1);
	}
		finally {
			DataBaseConexion.destroy(); 
		}
	//	finally {
	//		System.exit(0);
	//	}
	}
}
