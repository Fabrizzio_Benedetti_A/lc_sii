package cl.circle.localesconectados.enviosii.sql;

import java.io.Serializable;
import java.math.BigDecimal;

public class DocumentosAprobados implements Serializable {

	private static final long serialVersionUID = 1L;
	protected BigDecimal id; 
    protected String rutInformante;
    protected String dvInformante;
    protected String rutContribuyente;
    protected String dvContribuyente;
    protected String tipoDocumento;
    protected String fechaVenta;
    protected String totalMontoNeto;
    protected String totalMontoExento;
    protected String totalMontoTotal;
    protected String totalMontoPropina;
    protected String totalMontoVuelto;
    protected String totalMontoDonacion;
    protected String totalMontoTransaccion;
    protected String totalValesEmitidos;
    protected String identificadorEnvio;
    protected String canalTransaccion;
	   
    
    public DocumentosAprobados() {
    	
    }
        
	    /**
	 * @return the id
	 */
	public BigDecimal getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(BigDecimal id) {
		this.id = id;
	}

		/**
		 * @return the rutInformante
		 */
		public String getRutInformante() {
			return rutInformante;
		}
		/**
		 * @param rutInformante the rutInformante to set
		 */
		public void setRutInformante(String rutInformante) {
			this.rutInformante = rutInformante;
		}
		/**
		 * @return the dvInformante
		 */
		public String getDvInformante() {
			return dvInformante;
		}
		/**
		 * @param dvInformante the dvInformante to set
		 */
		public void setDvInformante(String dvInformante) {
			this.dvInformante = dvInformante;
		}
		/**
		 * @return the rutContribuyente
		 */
		public String getRutContribuyente() {
			return rutContribuyente;
		}
		/**
		 * @param rutContribuyente the rutContribuyente to set
		 */
		public void setRutContribuyente(String rutContribuyente) {
			this.rutContribuyente = rutContribuyente;
		}
		/**
		 * @return the dvContribuyente
		 */
		public String getDvContribuyente() {
			return dvContribuyente;
		}
		/**
		 * @param dvContribuyente the dvContribuyente to set
		 */
		public void setDvContribuyente(String dvContribuyente) {
			this.dvContribuyente = dvContribuyente;
		}
		/**
		 * @return the tipoDocumento
		 */
		public String getTipoDocumento() {
			return tipoDocumento;
		}
		/**
		 * @param tipoDocumento the tipoDocumento to set
		 */
		public void setTipoDocumento(String tipoDocumento) {
			this.tipoDocumento = tipoDocumento;
		}
		/**
		 * @return the fechaVenta
		 */
		public String getFechaVenta() {
			return fechaVenta;
		}
		/**
		 * @param fechaVenta the fechaVenta to set
		 */
		public void setFechaVenta(String fechaVenta) {
			this.fechaVenta = fechaVenta;
		}
		/**
		 * @return the totalMontoNeto
		 */
		public String getTotalMontoNeto() {
			return totalMontoNeto;
		}
		/**
		 * @param totalMontoNeto the totalMontoNeto to set
		 */
		public void setTotalMontoNeto(String totalMontoNeto) {
			this.totalMontoNeto = totalMontoNeto;
		}
		/**
		 * @return the totalMontoExento
		 */
		public String getTotalMontoExento() {
			return totalMontoExento;
		}
		/**
		 * @param totalMontoExento the totalMontoExento to set
		 */
		public void setTotalMontoExento(String totalMontoExento) {
			this.totalMontoExento = totalMontoExento;
		}
		/**
		 * @return the totalMontoTotal
		 */
		public String getTotalMontoTotal() {
			return totalMontoTotal;
		}
		/**
		 * @param totalMontoTotal the totalMontoTotal to set
		 */
		public void setTotalMontoTotal(String totalMontoTotal) {
			this.totalMontoTotal = totalMontoTotal;
		}
		/**
		 * @return the totalMontoPropina
		 */
		public String getTotalMontoPropina() {
			return totalMontoPropina;
		}
		/**
		 * @param totalMontoPropina the totalMontoPropina to set
		 */
		public void setTotalMontoPropina(String totalMontoPropina) {
			this.totalMontoPropina = totalMontoPropina;
		}
		/**
		 * @return the totalMontoVuelto
		 */
		public String getTotalMontoVuelto() {
			return totalMontoVuelto;
		}
		/**
		 * @param totalMontoVuelto the totalMontoVuelto to set
		 */
		public void setTotalMontoVuelto(String totalMontoVuelto) {
			this.totalMontoVuelto = totalMontoVuelto;
		}
		/**
		 * @return the totalMontoDonacion
		 */
		public String getTotalMontoDonacion() {
			return totalMontoDonacion;
		}
		/**
		 * @param totalMontoDonacion the totalMontoDonacion to set
		 */
		public void setTotalMontoDonacion(String totalMontoDonacion) {
			this.totalMontoDonacion = totalMontoDonacion;
		}
		/**
		 * @return the totalMontoTransaccion
		 */
		public String getTotalMontoTransaccion() {
			return totalMontoTransaccion;
		}
		/**
		 * @param totalMontoTransaccion the totalMontoTransaccion to set
		 */
		public void setTotalMontoTransaccion(String totalMontoTransaccion) {
			this.totalMontoTransaccion = totalMontoTransaccion;
		}
		/**
		 * @return the totalValesEmitidos
		 */
		public String getTotalValesEmitidos() {
			return totalValesEmitidos;
		}
		/**
		 * @param totalValesEmitidos the totalValesEmitidos to set
		 */
		public void setTotalValesEmitidos(String totalValesEmitidos) {
			this.totalValesEmitidos = totalValesEmitidos;
		}
		/**
		 * @return the identificadorEnvio
		 */
		public String getIdentificadorEnvio() {
			return identificadorEnvio;
		}
		/**
		 * @param identificadorEnvio the identificadorEnvio to set
		 */
		public void setIdentificadorEnvio(String identificadorEnvio) {
			this.identificadorEnvio = identificadorEnvio;
		}
		/**
		 * @return the canalTransaccion
		 */
		public String getCanalTransaccion() {
			return canalTransaccion;
		}
		/**
		 * @param canalTransaccion the canalTransaccion to set
		 */
		public void setCanalTransaccion(String canalTransaccion) {
			this.canalTransaccion = canalTransaccion;
		}
		/**
		 * @return the serialversionuid
		 */
		public static long getSerialversionuid() {
			return serialVersionUID;
		}

		@Override
		public String toString() {
			return "DocumentosAprobados [id=" + id + ", rutInformante=" + rutInformante + ", dvInformante="
					+ dvInformante + ", rutContribuyente=" + rutContribuyente + ", dvContribuyente=" + dvContribuyente
					+ ", tipoDocumento=" + tipoDocumento + ", fechaVenta=" + fechaVenta + ", totalMontoNeto="
					+ totalMontoNeto + ", totalMontoExento=" + totalMontoExento + ", totalMontoTotal=" + totalMontoTotal
					+ ", totalMontoPropina=" + totalMontoPropina + ", totalMontoVuelto=" + totalMontoVuelto
					+ ", totalMontoDonacion=" + totalMontoDonacion + ", totalMontoTransaccion=" + totalMontoTransaccion
					+ ", totalValesEmitidos=" + totalValesEmitidos + ", identificadorEnvio=" + identificadorEnvio
					+ ", canalTransaccion=" + canalTransaccion + "]";
		}
		
	    
}
