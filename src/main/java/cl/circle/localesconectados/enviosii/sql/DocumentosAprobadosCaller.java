package cl.circle.localesconectados.enviosii.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import cl.circle.localesconectados.service.DataBaseConexion;
import cl.circle.localesconectados.service.Utiles;

public class DocumentosAprobadosCaller extends ProcedureCaller {

	public static DocumentoAprobadosResult execute(Connection connection) throws SQLException {
		return new DocumentosAprobadosCaller().executeProc(connection);
	}

	private DocumentoAprobadosResult executeProc(Connection conn) throws SQLException {
		DocumentoAprobadosResult result = createProcResult();
		final CallableStatement call = prepareCall(conn, "{?=call sii.sp_Documentos_Aprobados_A_Enviar(?,?,?)}");
		try {
			  call.registerOutParameter(1, Types.INTEGER);
			   call.registerOutParameter(2, Types.INTEGER);
			  call.registerOutParameter(3, Types.NUMERIC);
	          call.registerOutParameter(4, Types.NVARCHAR);
	          int updateCount = 0;
	             
	             boolean haveRset = call.execute();
	             while (haveRset || updateCount != -1) {
	                 if (!haveRset) {
	                     updateCount = call.getUpdateCount();
	                 } else if (result.getDocumentos() == null) {
	                     result.setDocumentos(readDocumentos(call.getResultSet()));
	                 } else {
	                     unexpectedResultSet(call.getResultSet());
	                 }
	                 haveRset = call.getMoreResults();
	             }

			    result.setReturnValue(call.getInt(1));
			    result.setCantidad(call.getInt(2));
			    result.setResultCode(call.getBigDecimal(3));
		        result.setResultData(call.getString(4));
		}
		finally {
			conn.close();
		}
		return result; 
	}

	private DocumentosAprobados[] readDocumentos(ResultSet resultSet) throws SQLException {
		if (resultSet == null) return null;
		  
		try{
			final int cId						  = resultSet.findColumn("id");
		    final int cRutInformante			  = resultSet.findColumn("rutInformante");          
		    //final int cDvInformante               = resultSet.findColumn("dvInformante");           
		    final int cRutContribuyente           = resultSet.findColumn("rutContribuyente");       
		    //final int cDvContribuyente            = resultSet.findColumn("dvContribuyente");        
		    final int cTipoDocumento              = resultSet.findColumn("tipoDocumento");          
		    final int cFechaVenta                 = resultSet.findColumn("fechaVenta");             
		    final int cTotalMontoNeto             = resultSet.findColumn("totalMontoNeto");         
		    final int cTotalMontoExento           = resultSet.findColumn("totalMontoExento");       
		    final int cTotalMontoTotal            = resultSet.findColumn("totalMontoTotal");        
		    final int cTotalMontoPropina          = resultSet.findColumn("totalMontoPropina");      
		    final int cTotalMontoVuelto           = resultSet.findColumn("totalMontoVuelto");       
		    final int cTotalMontoDonacion         = resultSet.findColumn("totalMontoDonacion");     
		    final int cTotalMontoTransaccion      = resultSet.findColumn("totalMontoTransaccion");  
		    final int cTotalValesEmitidos         = resultSet.findColumn("totalValesEmitidos");     
		    final int cIdentificadorEnvio         = resultSet.findColumn("identificadorEnvio");     
		    final int cCanalTransaccion           = resultSet.findColumn("canalTransaccion"); 
		    
		    List <DocumentosAprobados> list = new ArrayList <DocumentosAprobados> ();
		    while (resultSet.next()) {
		    	DocumentosAprobados item = new DocumentosAprobados(); 
		    	String rutInfo[] = resultSet.getString(cRutInformante).split("-"); 
		    	String rutContribuyente = resultSet.getString(cRutContribuyente); 
		    	//String rutCont[] = resultSet.getString(cRutContribuyente).split("-"); 
		    	rutContribuyente = rutContribuyente.replaceAll("-", "");
		    	
				rutContribuyente = rutContribuyente.substring(0,rutContribuyente.length()-1)+"-"+rutContribuyente.substring(rutContribuyente.length()-1);

		    	
		    	String rutCont[] = rutContribuyente.split("-");
		    	
		    	item.setId(resultSet.getBigDecimal(cId));
		    	item.setRutInformante(rutInfo[0]);         
		    	item.setDvInformante(rutInfo[1]);
		    	item.setRutContribuyente(rutCont[0]);      
		    	item.setDvContribuyente(rutCont[1]);       
		    	item.setTipoDocumento(resultSet.getString(cTipoDocumento));         
		    	item.setFechaVenta(resultSet.getString(cFechaVenta)); 
		    	item.setTotalMontoNeto (resultSet.getString(cTotalMontoNeto));        
		    	item.setTotalMontoExento(resultSet.getString(cTotalMontoExento));      
		    	item.setTotalMontoTotal(resultSet.getString(cTotalMontoTotal));       
		    	item.setTotalMontoPropina(resultSet.getString(cTotalMontoPropina));     
		    	item.setTotalMontoVuelto(resultSet.getString(cTotalMontoVuelto));      
		    	item.setTotalMontoDonacion(resultSet.getString(cTotalMontoDonacion));    
		    	item.setTotalMontoTransaccion(resultSet.getString(cTotalMontoTransaccion)); 
		    	item.setTotalValesEmitidos(resultSet.getString(cTotalValesEmitidos));    
		    	item.setIdentificadorEnvio(resultSet.getString(cIdentificadorEnvio));    
		    	item.setCanalTransaccion(resultSet.getString(cCanalTransaccion));   
		    	
		    	if(verificaRut(rutContribuyente)) {
		    		list.add(item);
		    	}
		    	else {
		    		UpdateDocumentoNoValidoCaller.execute(DataBaseConexion.getConnection(), item.getId());
		    	}
		    }
		    return list.toArray(new DocumentosAprobados[list.size()]);
		}
		finally {
			resultSet.close();
		}
	}

	private boolean verificaRut(String rutCont) {
		return Utiles.validarRut(rutCont);
	}

	private DocumentoAprobadosResult createProcResult() {
		return new DocumentoAprobadosResult(); 
	}

}
