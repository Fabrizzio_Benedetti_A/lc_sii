package cl.circle.localesconectados.enviosii.sql;

import java.io.Serializable;
import java.math.BigDecimal;

public class DocumentosEnviados implements Serializable {

	private static final long serialVersionUID = 1L;
	protected BigDecimal id; 
    protected String rutInformante;
    protected String dvInformante;
    protected String rutContribuyente;
    protected String dvContribuyente;
    protected String trackID;
	/**
	 * @return the id
	 */
	public BigDecimal getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(BigDecimal id) {
		this.id = id;
	}
	/**
	 * @return the rutInformante
	 */
	public String getRutInformante() {
		return rutInformante;
	}
	/**
	 * @param rutInformante the rutInformante to set
	 */
	public void setRutInformante(String rutInformante) {
		this.rutInformante = rutInformante;
	}
	/**
	 * @return the dvInformante
	 */
	public String getDvInformante() {
		return dvInformante;
	}
	/**
	 * @param dvInformante the dvInformante to set
	 */
	public void setDvInformante(String dvInformante) {
		this.dvInformante = dvInformante;
	}
	/**
	 * @return the trackID
	 */
	public String getTrackID() {
		return trackID;
	}
	/**
	 * @param trackID the trackID to set
	 */
	public void setTrackID(String trackID) {
		this.trackID = trackID;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the rutContribuyente
	 */
	public String getRutContribuyente() {
		return rutContribuyente;
	}
	/**
	 * @param rutContribuyente the rutContribuyente to set
	 */
	public void setRutContribuyente(String rutContribuyente) {
		this.rutContribuyente = rutContribuyente;
	}
	/**
	 * @return the dvContribuyente
	 */
	public String getDvContribuyente() {
		return dvContribuyente;
	}
	/**
	 * @param dvContribuyente the dvContribuyente to set
	 */
	public void setDvContribuyente(String dvContribuyente) {
		this.dvContribuyente = dvContribuyente;
	}
	@Override
	public String toString() {
		return "DocumentosEnviados [id=" + id + ", rutInformante=" + rutInformante + ", dvInformante=" + dvInformante
				+ ", rutContribuyente=" + rutContribuyente + ", dvContribuyente=" + dvContribuyente + ", trackID="
				+ trackID + "]";
	}
    
    
}
