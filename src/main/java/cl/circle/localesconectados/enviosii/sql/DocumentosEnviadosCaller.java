package cl.circle.localesconectados.enviosii.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class DocumentosEnviadosCaller extends ProcedureCaller {

	public static DocumentosEnviadosResult execute(Connection connection) throws SQLException {
		return new DocumentosEnviadosCaller().executeProc(connection); 
	}

	private DocumentosEnviadosResult executeProc(Connection conn) throws SQLException {
		DocumentosEnviadosResult result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call sii.sp_Documentos_Enviados(?,?,?)}");
		try {
			  call.registerOutParameter(1, Types.INTEGER);
			   call.registerOutParameter(2, Types.INTEGER);
			  call.registerOutParameter(3, Types.NUMERIC);
	          call.registerOutParameter(4, Types.NVARCHAR);
	          int updateCount = 0;
	             
	             boolean haveRset = call.execute();
	             while (haveRset || updateCount != -1) {
	                 if (!haveRset) {
	                     updateCount = call.getUpdateCount();
	                 } else if (result.getDocumentosEnviados() == null) {
	                     result.setDocumentosEnviados(readDocumentos(call.getResultSet()));
	                 } else {
	                     unexpectedResultSet(call.getResultSet());
	                 }
	                 haveRset = call.getMoreResults();
	             }

			    result.setReturnValue(call.getInt(1));
			    result.setCantidad(call.getInt(2));
			    result.setResultCode(call.getBigDecimal(3));
		        result.setResultData(call.getString(4));
		}
		finally {
			conn.close();
		}
		return result; 
	}

	private DocumentosEnviados[] readDocumentos(ResultSet resultSet) throws SQLException {
		if (resultSet == null) return null;
		  
		try{
			final int cId						  = resultSet.findColumn("id");
		    final int cRutInformante			  = resultSet.findColumn("rutInformante");             
		    final int cRutContribuyente           = resultSet.findColumn("rutContribuyente");       
		    final int cTrackId                    = resultSet.findColumn("trackId");          
		    
		List <DocumentosEnviados> list = new ArrayList <DocumentosEnviados>(); 
		 while (resultSet.next()) {
			 DocumentosEnviados item = new DocumentosEnviados(); 
		    	String rutInfo[] = resultSet.getString(cRutInformante).split("-"); 
		    	//String rutCont[] = resultSet.getString(cRutContribuyente).split("-");
		    	String rutContribuyente = resultSet.getString(cRutContribuyente); 
		    		   rutContribuyente = rutContribuyente.replaceAll("-", "");
		    	   	   rutContribuyente = rutContribuyente.substring(0,rutContribuyente.length()-1)+"-"+rutContribuyente.substring(rutContribuyente.length()-1);

		    	String rutCont[] = rutContribuyente.split("-");
		    	
		    	item.setId(resultSet.getBigDecimal(cId));
		    	item.setRutInformante(rutInfo[0]);         
		    	item.setDvInformante(rutInfo[1]);
		    	item.setRutContribuyente(rutCont[0]);      
		    	item.setDvContribuyente(rutCont[1]);       
		    	item.setTrackID(resultSet.getString(cTrackId));
		    	
		    	list.add(item); 
		 }
		    return list.toArray(new DocumentosEnviados[list.size()]); 
		}	
		finally {
			resultSet.close();
		}
	}

	private DocumentosEnviadosResult createProcResult() {
		return new DocumentosEnviadosResult();
	}

}
