package cl.circle.localesconectados.enviosii.sql;

import java.math.BigDecimal;

public class DocumentosEnviadosResult extends ProcedureResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	private int returnValue;
	private BigDecimal resultCode;
	private String resultData;
	private int cantidad; 
	
	private DocumentosEnviados[] documentosEnviados;

	/**
	 * @return the returnValue
	 */
	public int getReturnValue() {
		return returnValue;
	}

	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}

	/**
	 * @return the resultCode
	 */
	public BigDecimal getResultCode() {
		return resultCode;
	}

	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}

	/**
	 * @return the resultData
	 */
	public String getResultData() {
		return resultData;
	}

	/**
	 * @param resultData the resultData to set
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}

	/**
	 * @return the cantidad
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the documentosEnviados
	 */
	public DocumentosEnviados[] getDocumentosEnviados() {
		return documentosEnviados;
	}

	/**
	 * @param documentosEnviados the documentosEnviados to set
	 */
	public void setDocumentosEnviados(DocumentosEnviados[] documentosEnviados) {
		this.documentosEnviados = documentosEnviados;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	} 
	

}
