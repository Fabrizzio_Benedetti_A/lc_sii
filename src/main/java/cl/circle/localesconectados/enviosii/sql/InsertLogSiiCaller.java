package cl.circle.localesconectados.enviosii.sql;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class InsertLogSiiCaller extends ProcedureCaller {

	public static void execute(Connection connection, BigDecimal id, String metodo, String request) throws SQLException {
		new InsertLogSiiCaller().executeProc(connection, id, metodo, request);
	}

	private void executeProc(Connection conn, BigDecimal id, String metodo, String request) throws SQLException {
		final CallableStatement call = prepareCall(conn, "{?=call sistema.sp_Log_SII_Insert(?,?,?)}");
		try {
			  call.registerOutParameter(1, Types.INTEGER);
			  call.setBigDecimal(2, id);
			  call.setString(3, metodo);
			  call.setString(4, request);
	         
	         
	          call.execute();
		}
		finally {
			conn.close();
		}
	}
	
	public static void execute(Connection connection, String ids, String metodo, String request) throws SQLException {
		new InsertLogSiiCaller().executeProc(connection, ids, metodo, request);
	}

	private void executeProc(Connection conn, String ids, String metodo, String request) throws SQLException {
		final CallableStatement call = prepareCall(conn, "{?=call sistema.sp_Log_Ids_SII_Insert(?,?,?)}");
		try {
			  call.registerOutParameter(1, Types.INTEGER);
			  call.setString(2, ids);
			  call.setString(3, metodo);
			  call.setString(4, request);
	        
	         
	          call.execute();
		}
		finally {
			conn.close();
		}
		
	}
}
