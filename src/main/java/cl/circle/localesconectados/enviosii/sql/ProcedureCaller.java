/*
 * Source: ProcedureCaller.java - 
 * Author: Fabrizzio Benedetti A.
 *

 */

package cl.circle.localesconectados.enviosii.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Methods common to all procedure callers.
 *
 * @author Fabrizzio Benedetti A.
 */
public abstract class ProcedureCaller
{
    /**
     * Same as <code>oracle.jdbc.OracleTypes.CURSOR</code>.
     */
    protected static final int ORACLE_CURSOR = -10;

    /**
     * Constructs a new instance.
     */
    public ProcedureCaller()
    {
    }

    /**
     * Constructs a CallableStatement using a connection and statement.
     *
     * @param  conn the connection to the database.
     * @param  sql the statement to send to the database.
     * @return a new CallableStatement instance.
     * @throws NullPointerException if conn or sql are null.
     * @throws SQLException if an SQL error occurs.
     */
    protected CallableStatement prepareCall(Connection conn, String sql)
        throws SQLException
    {
        return conn.prepareCall(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    }

    /**
     * Converts a Date to a Timestamp instance.
     *
     * @param  date the Date to be converted (maybe null).
     * @return a Timestamp instance or null.
     */
    protected Timestamp dateToTimestamp(Date date)
    {
        if (date == null || date instanceof Timestamp)
            return (Timestamp) date;
        return new Timestamp(date.getTime());
    }
    /**
     * Returns the JDBC URL of the supplied connection.
     *
     * @param  conn the connection to the database.
     * @return the JDBC URL of the supplied connection.
     * @throws NullPointerException if conn is null.
     * @throws SQLException if an SQL error occurs.
     */
    protected String getJdbcURL(final Connection conn)
        throws SQLException
    {
        final String url = conn.getMetaData().getURL();
        return (url == null || url.isEmpty()) ? "jdbc:unknown:" : url;
    }

    /**
     * Called when an unexpected ResultSet is received by a Caller.
     *
     * @param  resultSet the ResultSet (maybe null).
     * @throws SQLException if an SQL error occurs.
     */
    protected void unexpectedResultSet(ResultSet resultSet)
        throws SQLException
    {
        if (resultSet != null) {
            resultSet.close();
        }
    }
    /**
     * Returns the {@link Integer} value of the supplied object value.
     *
     * @param  value the object value to convert to an {@link Integer}.
     * @return the Integer or {@code null} if {@code value} is {@code null}.
     * @throws IllegalArgumentException if {@code value} is not a {@link Number}.
     */
    protected Integer objectToInteger(final Object value)
    {
        if (value == null)
            return null;
        if (value instanceof Integer)
            return (Integer) value;
        if (value instanceof Number)
            return new Integer(((Number) value).intValue());
        throw new IllegalArgumentException("Object is not a number: " + value);
    }
}
