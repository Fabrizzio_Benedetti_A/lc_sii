/*
 * Source: ProcedureResult.java - 
 * Author: Fabrizzio Benedetti A.
 *

 */

package cl.circle.localesconectados.enviosii.sql;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlType;

/**
 * Methods common to all procedure results.
 *
 * @author Fabrizzio Benedetti A.
 */
@XmlType(name = "ProcedureResult")
public abstract class ProcedureResult implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new instance.
     */
    public ProcedureResult()
    {
    }
    
   
}
