package cl.circle.localesconectados.enviosii.sql;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class UpdateDocumentoNoValidoCaller extends ProcedureCaller {
	
	
	public static UpdateEnvioSii execute(Connection connection, BigDecimal id) throws SQLException {
		return new UpdateDocumentoNoValidoCaller().executeProc(connection, id); 
	}

	private UpdateEnvioSii executeProc(Connection conn, BigDecimal id) throws SQLException {
		UpdateEnvioSii result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call sii.sp_Documento_No_Valido(?,?,?)}");
		try {
			  call.registerOutParameter(1, Types.INTEGER);
			  call.setBigDecimal(2, id);
			  call.registerOutParameter(3, Types.NUMERIC);
	          call.registerOutParameter(4, Types.NVARCHAR);
	         
	          call.execute();
	        	  
			    result.setReturnValue(call.getInt(1));
		
			    result.setResultCode(call.getBigDecimal(3));
		        result.setResultData(call.getString(4));
		}
		finally {
			conn.close();
		}
		
		return result; 
	}

	private UpdateEnvioSii createProcResult() {
		return new UpdateEnvioSii(); 
	}

}
