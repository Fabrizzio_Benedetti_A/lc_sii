package cl.circle.localesconectados.enviosii.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class UpdateEnvioSiiCaller extends ProcedureCaller {

	public static UpdateEnvioSii execute(Connection connection, String ids, int track) throws SQLException {
		return new UpdateEnvioSiiCaller().executeProc(connection, ids, track); 
	}

	private UpdateEnvioSii executeProc(Connection conn, String ids, int track) throws SQLException {
		UpdateEnvioSii result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call sii.sp_Documentos_Actualizar_Estado(?,?,?,?)}");
		try {
			  call.registerOutParameter(1, Types.INTEGER);
			  call.setString(2, ids);
			  call.setInt(3, track);
			  call.registerOutParameter(4, Types.NUMERIC);
	          call.registerOutParameter(5, Types.NVARCHAR);
	         
	          call.execute();
	        	  
			    result.setReturnValue(call.getInt(1));
		
			    result.setResultCode(call.getBigDecimal(4));
		        result.setResultData(call.getString(5));
		}
		finally {
			conn.close();
		}
		
		return result; 
	}

	private UpdateEnvioSii createProcResult() {
		return new UpdateEnvioSii(); 
	}

}
