package cl.circle.localesconectados.enviosii.sql;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class UpdateEnvioSiiEstadoCaller extends ProcedureCaller {

	public static UpdateEnvioSii execute(Connection connection, BigDecimal id, int track) throws SQLException {
		return new UpdateEnvioSiiEstadoCaller().executeProc(connection, id, track);
	}

	private UpdateEnvioSii executeProc(Connection conn, BigDecimal id, int track) throws SQLException {
		UpdateEnvioSii result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call sii.sp_Documentos_Actualizar_Estado_Sii(?,?,?,?)}");
		try {
			  call.registerOutParameter(1, Types.INTEGER);
			  call.setBigDecimal(2, id);
			  call.setInt(3, track);
			  call.registerOutParameter(4, Types.NUMERIC);
	          call.registerOutParameter(5, Types.NVARCHAR);
	         
	          call.execute();
	        	  
			    result.setReturnValue(call.getInt(1));
		
			    result.setResultCode(call.getBigDecimal(4));
		        result.setResultData(call.getString(5));
		}
		finally {
			conn.close();
		}
		
		return result; 
	}

	private UpdateEnvioSii createProcResult() {
		return new UpdateEnvioSii(); 
	}


}
