package cl.circle.localesconectados.imp;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import cl.circle.localesconectados.enviosii.EnvioSII;
import cl.circle.localesconectados.enviosii.sql.DocumentosEnviados;
import cl.circle.localesconectados.enviosii.sql.DocumentosEnviadosCaller;
import cl.circle.localesconectados.enviosii.sql.DocumentosEnviadosResult;
import cl.circle.localesconectados.enviosii.sql.InsertLogSiiCaller;
import cl.circle.localesconectados.enviosii.sql.UpdateEnvioSii;
import cl.circle.localesconectados.enviosii.sql.UpdateEnvioSiiEstadoCaller;
import cl.circle.localesconectados.service.ConexionSii;
import cl.circle.localesconectados.service.DataBaseConexion;
import cl.sii.sdi.oia.comprobanteboleta.ws.ComprobanteBoletaService;
import cl.sii.sdi.oia.comprobanteboleta.ws.ComprobanteBoletaService_Service;
import cl.sii.sdi.oia.comprobanteboleta.ws.TransaccionRespConCanalTo;

public class ConsultaEstadoDocumentos implements Serializable, Runnable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean log= false; 
	private ComprobanteBoletaService_Service service;
	private ComprobanteBoletaService port;
	
	public ConsultaEstadoDocumentos() throws MalformedURLException {
		this.log = EnvioSII.isLog();
		URL urlWS = new URL(EnvioSII.getWsEnvio()); 
		
		service = new ComprobanteBoletaService_Service(urlWS); 
		
		port = service.getComprobanteBoletaServiceWsEndpointImplPort(); 
		
		
		final BindingProvider bindingProvider = (BindingProvider) port;
		
	
		final Binding binding = bindingProvider.getBinding();
	  //  final Map<String,Object> reqctx = bindingProvider.getRequestContext();
	    final WebHandler webHandler = new WebHandler();
		
	     
	    // Append Security Web Handler to WebService Handler Chain
	       final List<Handler> newHandlerChain = new ArrayList<>(4);
	       final List<Handler> oldHandlerChain = binding.getHandlerChain();
	       if (oldHandlerChain != null) newHandlerChain.addAll(oldHandlerChain);
	       newHandlerChain.add(webHandler);
	       binding.setHandlerChain(newHandlerChain);
	}
	
	public void consulta() {
		try {
			ConexionSii cs = new ConexionSii(); 
			String token = cs.autentica("",""); 
			
			TokenSii.tokenValue= token; 
			
			System.out.println(port.getVersion());
			int envio= 1; 
		    while(true) {
			//buscar en la BD los enviados y su estado en el sii por numero de envio 
			DocumentosEnviadosResult result = DocumentosEnviadosCaller.execute(DataBaseConexion.getConnection()); 
			if (result.getCantidad()==0) {
	 			if (this.log) {
	 			if (envio==1) {
	 			System.out.println("No Existen documntos para consultar.");
	 				}
	 			else {
	 				System.out.println("No Quedan documntos para consultar.");
	 				}
	 			}
	 			break; 
	 		}
			
			for (DocumentosEnviados consulta: result.getDocumentosEnviados()) {
				if(this.log) {
					System.out.println(consulta.toString());	
				}
				InsertLogSiiCaller.execute(DataBaseConexion.getConnection(), consulta.getId(), "CONSULTA ESTADO", consulta.toString());
				
				TransaccionRespConCanalTo respuesta = port.consultaEstadoInfoPorTrackIdConCanal(consulta.getRutInformante(), consulta.getDvInformante(), consulta.getTrackID());
				if(this.log) {
					System.out.println(respuesta.toString());	
				}
				InsertLogSiiCaller.execute(DataBaseConexion.getConnection(), consulta.getId(), "RESPUESTA ESTADO", respuesta.toString());
				//if (respuesta.getCodResp()==0) {
				if (!Objects.isNull(respuesta.getTrackid())) {
					UpdateEnvioSii resultInsert = UpdateEnvioSiiEstadoCaller.execute(DataBaseConexion.getConnection(), consulta.getId(), respuesta.getTrackid().intValue()); 
 				if(this.log)
 					System.out.println(resultInsert.getResultData());
		 			}
		 		envio++; 
				}
		    }
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		this.consulta();
	}

}
