package cl.circle.localesconectados.imp;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;


import cl.circle.localesconectados.enviosii.EnvioSII;
import cl.circle.localesconectados.enviosii.sql.DocumentoAprobadosResult;
import cl.circle.localesconectados.enviosii.sql.DocumentosAprobados;
import cl.circle.localesconectados.enviosii.sql.DocumentosAprobadosCaller;
import cl.circle.localesconectados.enviosii.sql.InsertLogSiiCaller;
import cl.circle.localesconectados.enviosii.sql.UpdateEnvioSii;
import cl.circle.localesconectados.enviosii.sql.UpdateEnvioSiiCaller;
import cl.circle.localesconectados.service.ConexionSii;
import cl.circle.localesconectados.service.DataBaseConexion;
import cl.sii.sdi.oia.comprobanteboleta.ws.ComprobanteBoletaService;
//import cl.sii.sdi.oia.comprobanteboleta.ws.ComprobanteBoletaService_PortType;
import cl.sii.sdi.oia.comprobanteboleta.ws.ComprobanteBoletaService_Service;
//import cl.sii.sdi.oia.comprobanteboleta.ws.ComprobanteBoletaService_ServiceLocator;
import cl.sii.sdi.oia.comprobanteboleta.ws.RespuestaTransaccionesTo;
import cl.sii.sdi.oia.comprobanteboleta.ws.TransaccionConCanalTo;


public class GenerarEnvioDocumentos implements Serializable, Runnable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean log= false; 
	
	private ComprobanteBoletaService_Service service;
	private ComprobanteBoletaService port;
	
	public GenerarEnvioDocumentos () throws MalformedURLException {
		this.log = EnvioSII.isLog();
		
		
		URL urlWS = new URL(EnvioSII.getWsEnvio()); 
		
		service = new ComprobanteBoletaService_Service(urlWS); 
		
		port = service.getComprobanteBoletaServiceWsEndpointImplPort(); 
		
		
		final BindingProvider bindingProvider = (BindingProvider) port;
		
	
		final Binding binding = bindingProvider.getBinding();
	  //  final Map<String,Object> reqctx = bindingProvider.getRequestContext();
	    final WebHandler webHandler = new WebHandler();
		
	     
	    // Append Security Web Handler to WebService Handler Chain
	       final List<Handler> newHandlerChain = new ArrayList<>(4);
	       final List<Handler> oldHandlerChain = binding.getHandlerChain();
	       if (oldHandlerChain != null) newHandlerChain.addAll(oldHandlerChain);
	       newHandlerChain.add(webHandler);
	       binding.setHandlerChain(newHandlerChain);
		
	}

	public void generar() {
		//buscar los documentos en la bd para ser enviados 
		// se pueden enviar hasta 50 por vez, asi que se crea un loop de 	
		//envios en la consulta de 50 en 50
		try {
			
	
			ConexionSii cs = new ConexionSii(); 
			String token = cs.autentica("",""); 
			
			TokenSii.tokenValue= token; 
			
		//test
			System.out.println(port.getVersion());
		
		int envio= 1; 
	  
	    while(true) {  
	    	String ids = ""; 
		 		DocumentoAprobadosResult result = DocumentosAprobadosCaller.execute(DataBaseConexion.getConnection()) ;
		 		if (result.getCantidad()==0) {
		 			if (this.log) {
		 			if (envio==1) {
		 			System.out.println("No Existen documntos para enviar.");
		 				}
		 			else {
		 				System.out.println("No Quedan documntos para enviar.");
		 				}
		 			}
		 			break; 
		 		}
		 			
		 			List<TransaccionConCanalTo> transacciones = new ArrayList <TransaccionConCanalTo>();
		 				for (DocumentosAprobados documento : result.getDocumentos()) {
			 				TransaccionConCanalTo item = new TransaccionConCanalTo();
				 				item.setRutInformante(documento.getRutInformante());
				 				item.setDvInformante(documento.getDvInformante());
				 				item.setRutContribuyente(documento.getRutContribuyente());
				 				item.setDvContribuyente(documento.getDvContribuyente());
				 				item.setTipoDocumento(documento.getTipoDocumento());
				 				item.setFechaVenta(documento.getFechaVenta());
				 				item.setTotalMontoNeto(documento.getTotalMontoNeto());
				 				item.setTotalMontoExento(documento.getTotalMontoExento());
				 				item.setTotalMontoTotal(documento.getTotalMontoTotal());
				 				item.setTotalMontoPropina(documento.getTotalMontoPropina());
				 				item.setTotalMontoVuelto(documento.getTotalMontoVuelto());
				 				item.setTotalMontoDonacion(documento.getTotalMontoDonacion());
				 				item.setTotalMontoTransaccion(documento.getTotalMontoTransaccion());
				 				item.setTotalValesEmitidos(documento.getTotalValesEmitidos());
				 				//item.setTotalValesEmitidos(String.valueOf(result.getCantidad()));
				 				item.setCanalTransaccion(documento.getCanalTransaccion());
				 				item.setIdentificadorEnvio(documento.getIdentificadorEnvio());
				 				item.setCanalTransaccion(documento.getCanalTransaccion());
				 				
				 				ids = ids+""+documento.getId().intValue()+"@";
				 				
				 				
				 				InsertLogSiiCaller.execute(DataBaseConexion.getConnection(), documento.getId(),  "DOCUMENTO A ENVIAR", item.toString());
				 			
			 				if(this.log)
			 					System.out.println(item.toString());
		 			    	transacciones.add(item);
		 				}	
		 				
		 				if (transacciones.isEmpty()) {
		 					System.out.println("No Existen documntos para enviar.");
		 					break; 
		 				}
		 				
		 				RespuestaTransaccionesTo respuestaIngreso = port.ingresarTransaccionesConCanal(transacciones);
		 				
		 				if(this.log) {
		 					System.out.println("envio numero ["+envio+"]");
		 					System.out.println ("codResp:"   + respuestaIngreso.getCodResp()); 
			 				System.out.println ("Descr:"     + respuestaIngreso.getDescResp());
			 				//el track simpre es el mismo para un mismo envio 
			 			//	System.out.println ("track:"     + Arrays.toString(respuestaIngreso.getTracksID().get(0)));
			 				System.out.println ("track:"     + respuestaIngreso.getTracksID());
			 				System.out.println ("ccbt:"      + respuestaIngreso.getCcbtTrackid());
			 				System.out.println ("respuesta:" + respuestaIngreso.toString()); 
		 				}
		 				//si es 0 el envio fue enviado correctamente 
		 				if (respuestaIngreso.getCodResp()==0) {
		 					String[] idArray = ids.split("@"); 
		 					for (int index =0; index < idArray.length; index++) {
		 						
		 						int idEnvio = respuestaIngreso.getTracksID().get(index).intValue();
		 						UpdateEnvioSii resultInsert = UpdateEnvioSiiCaller.execute(DataBaseConexion.getConnection(), idArray[index], idEnvio); 
				 				if(this.log)
				 				System.out.println("ID"+idArray[index]+", IDEnvio"+idEnvio+",Result:"+resultInsert.getResultData());
			 				}
		 				}
		 				InsertLogSiiCaller.execute(DataBaseConexion.getConnection(), ids.replace("@", ","), "RESPUESTA ENVIO", respuestaIngreso.toString());
		 				envio++; 
		 		}
	    
			}
		catch(Exception e) {
			  e.printStackTrace();
		}
	}

	@Override
	public void run() {
		generar();
		
	}
	

}
