package cl.circle.localesconectados.imp;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.ws.ProtocolException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class WebHandler implements SOAPHandler<SOAPMessageContext> {
	  private final Set<QName> soapHeaders;

	    //--------------------------------------------------------------------------
	    //-- Constructor Methods ---------------------------------------------------
	    //--------------------------------------------------------------------------

	    /**
	     * Constructs a new {@code WebHandler} instance.
	     *
	     * @param  loginUser the current DesktopFX login user.
	     * @param  webContext the desktop WebService context.
	     * @throws NullPointerException if an argument is {@code null}.
	     */
	    WebHandler()
	    {

	        // Define SOAP Headers processed by this Handler
	    	//System.out.println("entrando al constructor");
	        soapHeaders = Collections.singleton(SECURITY_QN);
	    }

	    //--------------------------------------------------------------------------
	    //-- SOAPHandler Interface Methods -----------------------------------------
	    //--------------------------------------------------------------------------

	    /**
	     * Called at the conclusion of a message exchange pattern.
	     *
	     * @param context the SOAP handler message context.
	     */
	    @Override
	    public void close(final MessageContext context)
	    {
	        // Do nothing
	    }

	    /**
	     * Returns the headers that can be processed by this Handler.
	     *
	     * @return the headers that can be processed by this Handler.
	     */
	    @Override
	    public Set<QName> getHeaders()
	    {
	        return soapHeaders;
	    }

	    /**
	     * This method is invoked for fault message processing.
	     *
	     * @param  context the SOAP handler message context.
	     * @return {@code true} if the fault was handled.
	     */
	    @Override
	    public boolean handleFault(final SOAPMessageContext context)
	    {

	        return true;
	    }

	    /**
	     * Invoked for normal processing of inbound and outbound SOAP messages.
	     *
	     * @param  context the SOAP handler message context.
	     * @return {@code true} if the message was handled.
	     * @throws ProtocolException if an error occurs handling the message.
	     */
	    @Override
	    public boolean handleMessage(final SOAPMessageContext context)
	    {
	        try {
	        	//System.out.println("cargando handle");
	            final boolean outbound = (Boolean)
	                context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
	            if (outbound) {
	                
	            	String tokenSii= "TOKEN=";
	            	
	            	tokenSii+=TokenSii.tokenValue;
	            	System.out.println("tokenSii:"+tokenSii);
	            	//if (pepe.NonceValue!="") 
	                addSoapHttpHeaders(context, tokenSii);
	                
	                
	            } 
	            
	            return true;
	        } catch (final Throwable thrown) {
	            throw new ProtocolException(thrown.getMessage(), thrown);
	        }
	    }

	    //--------------------------------------------------------------------------
	    //-- HTTP Header Methods ---------------------------------------------------
	    //--------------------------------------------------------------------------

	    // Values of HTTP/SOAP headers
	    private static final String LANGUAGE_NAME =
	        Locale.getDefault().toString().replace('_', '-');
	    private static final List<String> LANGUAGE_LIST =
	        Collections.singletonList(LANGUAGE_NAME);
	    private static final List<String> ENCODING_LIST =
	        Collections.singletonList("gzip");

	    /**
	     * Add the security header fields to the supplied SOAP Message Context.
	     *
	     * @param  context the SOAP handler message context.
	     * @param  webAuth the Authorization Header for this request.
	     * @throws NullPointerException if an argument is {@code null}.
	     */
	    @SuppressWarnings("unchecked")
	    private void addSoapHttpHeaders(final SOAPMessageContext context, final String Diggest)
	    {
	        // Obtain or create the Web Service HTTP request headers
	        Map<String, List<String>> rheaders = (Map<String, List<String>>)
	            context.get(MessageContext.HTTP_REQUEST_HEADERS);
	        if (rheaders == null) {
	            rheaders = new HashMap<>(10);
	            context.put(MessageContext.HTTP_REQUEST_HEADERS, rheaders);
	        }

	        // Add standard "Authorization" header to the HTTP request
	        //rheaders.put("Authorization", Collections.singletonList(Diggest));

	        rheaders.put("Cookie", Collections.singletonList(Diggest));
	        
	        // Add standard "Accept-Language" header to the HTTP request
	        rheaders.put("Accept-Language", LANGUAGE_LIST);

	        // Add standard "Accept-Encoding" header to the HTTP request
	        rheaders.put("Accept-Encoding", ENCODING_LIST);
	    }

	    //--------------------------------------------------------------------------
	    //-- SOAP Header Methods ---------------------------------------------------
	    //--------------------------------------------------------------------------

	    // Security namespaces and types
	    private static final String WSSE_NS =
	        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	    private static final String WSU_NS =
	        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	    private static final String PASSWORD_TYPE =
	        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest";

	    // Qnames needed to build a security header
	    private static final QName SECURITY_QN = new QName(WSSE_NS, "Security", "wsse");
	    private static final QName USRTOKEN_QN = new QName(WSSE_NS, "UsernameToken", "wsse");
	    private static final QName USERNAME_QN = new QName(WSSE_NS, "Username", "wsse");
	    private static final QName PASSWORD_QN = new QName(WSSE_NS, "Password", "wsse");
	    private static final QName PASSTYPE_QN = new QName(null, "Type");
	    private static final QName NONCE_QN = new QName(WSSE_NS, "Nonce", "wsse");
	    private static final QName CREATED_QN = new QName(WSU_NS, "Created", "wsu");

	}