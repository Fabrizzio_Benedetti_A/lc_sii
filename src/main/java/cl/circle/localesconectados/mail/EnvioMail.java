package cl.circle.localesconectados.mail;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.mail.MessagingException;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;

import cl.circle.localesconectados.enviosii.EnvioSII;

public class EnvioMail {
	
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	
	public EnvioMail() {
		
	}
	
	public void envioMail(String email, String cc, String asunto, String mensaje, String archivoNombre) throws GeneralSecurityException, IOException, MessagingException {
		 Gmail service = generateGmailCredentials();
		    File file = new File (archivoNombre);
		    service.users()
		      .messages()
		      .send("me", 
		      		MailMessage.createMessage(MailMessage.createEmailWithAttachment(email,
		      				                                          EnvioSII.getEmail(),
		      				                                          cc,
		      				                                          asunto, 
		      				                                          mensaje,
		      				                                          file,
		      				                                          false)))
		      .execute();

	}
	
	 private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
	        GoogleCredential credential = new GoogleCredential.Builder()
	                .setTransport(HTTP_TRANSPORT)
	                .setJsonFactory(JSON_FACTORY)
	                .setClientSecrets(EnvioSII.getClientId(), EnvioSII.getSecret())
	                .build();

	        credential.setAccessToken(EnvioSII.getToken());
	        credential.setRefreshToken(EnvioSII.getRefresh());
	        return credential;
	    }
	 
	   public static Gmail generateGmailCredentials() throws GeneralSecurityException, IOException {
	        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

	        Credential cred = getCredentials(HTTP_TRANSPORT); 
	        
	        return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, cred)
	                .setApplicationName(EnvioSII.getAppName())
	                .build();
	    }
}
