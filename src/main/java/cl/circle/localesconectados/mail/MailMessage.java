package cl.circle.localesconectados.mail;

import com.google.api.client.util.Base64;
import com.google.api.services.gmail.model.Message;

import javax.activation.DataHandler;
import javax.activation.DataSource; 
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class MailMessage {
	
    public static MimeMessage createEmail(String to, String from, String cc, String subject, String body, boolean isHTML) throws MessagingException {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from));

        email.addRecipient(javax.mail.Message.RecipientType.TO,
                new InternetAddress(to));
        if (cc!= null) {
        email.addRecipient(javax.mail.Message.RecipientType.CC,
                new InternetAddress(cc));
        }
        
        email.setSubject(subject);

        if (isHTML) {
            email.setContent(body, "text/html; charset=utf-8");
        }
        else{
            email.setText(body);
        }
        return email;
    }

    public static MimeMessage createEmailWithAttachment(String to,
                                                        String from,
                                                        String cc,
                                                        String subject,
                                                        String body,
                                                        File file,
                                                        boolean isHTML)
            throws MessagingException {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);
        //de
        email.setFrom(new InternetAddress(from));
        
        //para
        InternetAddress[] iAdressArrayTo = InternetAddress.parse(to);
        email.addRecipients(javax.mail.Message.RecipientType.TO,
        		iAdressArrayTo);
        //cc
        if (cc!= null) {
        InternetAddress[] iAdressArrayCC = InternetAddress.parse(cc);
        email.setRecipients(javax.mail.Message.RecipientType.CC, iAdressArrayCC);
        }
  
        
       
        email.setSubject(subject);

        MimeBodyPart mimeBodyPart = new MimeBodyPart();


        if (isHTML) {
            mimeBodyPart.setContent(body, "text/html; charset=utf-8");
        }
        else{
            mimeBodyPart.setContent(body, "text/plain");
        }


        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);

        mimeBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(file);

        mimeBodyPart.setDataHandler(new DataHandler(source));
        mimeBodyPart.setFileName(file.getName());

        multipart.addBodyPart(mimeBodyPart);
        email.setContent(multipart);

        return email;
    }

    public static Message createMessage(MimeMessage mimeMessage) throws IOException, MessagingException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        mimeMessage.writeTo(buffer);
        byte[] bytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
        Message message = new Message();
        message.setRaw(encodedEmail);
        return message;
    }
}
