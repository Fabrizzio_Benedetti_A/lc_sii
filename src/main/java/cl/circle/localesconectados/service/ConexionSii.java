package cl.circle.localesconectados.service;

import java.io.CharArrayReader;
import java.io.Reader;
import java.io.StringWriter;

import java.rmi.RemoteException;

import java.security.KeyStore;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import cl.circle.localesconectados.enviosii.EnvioSII;
import cl.circle.sii.seed.CrSeedProxy;
import cl.circle.sii.token.GetTokenFromSeedProxy;


public class ConexionSii {

	public String autentica(String rutEmisor, String usuario)
	{
			
		CrSeedProxy proxySeed = new CrSeedProxy(EnvioSII.getWsSeed());
		GetTokenFromSeedProxy	proxyToken = new GetTokenFromSeedProxy(EnvioSII.getWsToken());
		
		StringBuffer estado  = new StringBuffer();
		StringBuffer glosa   = new StringBuffer();
		StringBuffer token   = new StringBuffer();
		StringBuffer semilla = new StringBuffer();
		String aliasKeyStore = "localesConectados";
		String passProtection = EnvioSII.getCertificadoPass(); //"15pilargoycoolea";
		String passCertificado = EnvioSII.getCertificadoPass(); //"15pilargoycoolea";
	
		KeyStore ks =null;
		try{
			ks = KeyStoreCrea.keyStoreTest(EnvioSII.getCertificadoRuta(), passCertificado, aliasKeyStore, passProtection); 
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();

		}	
		try 
		{
			String seedXml = proxySeed.getSeed();
		
			
			parseXmlSemilla( seedXml, estado, semilla );
			System.out.println("estado  : " + estado );
			System.out.println("semilla : " + semilla );
			
			// Se construye requerimiento XML con semilla
			String xmlToken       = getXmlToken( semilla.toString() );
			
			System.out.println("xmlToken : " + xmlToken );
			//Se firma requerimiento XML
	    
//			Enumeration<String> aliases = ks.aliases();
//		    while (aliases.hasMoreElements()) {
//		       String alias = aliases.nextElement();
//		        System.out.println("alias certificados :"+alias);
//		    }
	
			
			String signedXmlToken = GenEnveloped.firma(xmlToken	
					,aliasKeyStore 
					,passProtection
					,ks);
		//	System.out.println("signedXmlToken : " + signedXmlToken );
			String respuestaGetToken = proxyToken.getToken(signedXmlToken);
			
		//	System.out.println("respuestaGetToken : " + respuestaGetToken );
			estado.delete(0, estado.length());
			 
			parseXmlToken( respuestaGetToken, estado, glosa, token );
			
			System.out.println(token.toString()) ; 	
			return token.toString();	
		} 
		catch (RemoteException e) 
		{

			e.printStackTrace();
		}
		catch (Exception e) 
		{
		
			e.printStackTrace();
		}	
		
		return null;
	}
	// XXX TEST
	public String autenticaProd(String rutEmisor, String usuario)
	{
		CrSeedProxy 			proxySeed  = new CrSeedProxy("https://palena.sii.cl/DTEWS/CrSeed.jws?WSDL");
		GetTokenFromSeedProxy	proxyToken = new GetTokenFromSeedProxy("https://palena.sii.cl/DTEWS/GetTokenFromSeed.jws?WSDL");
		
	
		
		StringBuffer estado  = new StringBuffer();
		StringBuffer glosa   = new StringBuffer();
		StringBuffer token   = new StringBuffer();
		StringBuffer semilla = new StringBuffer();
		String aliasKeyStore = "localesConectados";
		String passProtection = "15pilargoycoolea";
		String passCertificado = "15pilargoycoolea";
	
		KeyStore ks =null;
		try{
		
			ks = KeyStoreCrea.keyStoreTest("D:\\dev\\desarrollo\\Eclipse\\LOCALES_CONECTADOS\\LC_SII\\CertificadoE-CertchilePGF.pfx",passCertificado, aliasKeyStore, passProtection);
			
		}
		catch (Exception e) 
		{
			
			e.printStackTrace();

		}	
		try 
		{
			String seedXml = proxySeed.getSeed();
		
			
			parseXmlSemilla( seedXml, estado, semilla );
			System.out.println("estado  : " + estado );
			System.out.println("semilla : " + semilla );
			
			// Se construye requerimiento XML con semilla
			String xmlToken       = getXmlToken( semilla.toString() );
			
			System.out.println("xmlToken : " + xmlToken );
			String signedXmlToken = GenEnveloped.firma(xmlToken	
					,aliasKeyStore 
					,passProtection
					,ks);
	
			String respuestaGetToken = proxyToken.getToken(signedXmlToken);
			
		//	System.out.println("respuestaGetToken : " + respuestaGetToken );
			estado.delete(0, estado.length());
			 
			parseXmlToken( respuestaGetToken, estado, glosa, token );
			
			System.out.println(token.toString()) ; 	
			return token.toString();	
		} 
		catch (RemoteException e) 
		{

			e.printStackTrace();
		}
		catch (Exception e) 
		{
		
			e.printStackTrace();
		}	
		
		return null;
	}
	
	public static boolean parseXmlSemilla( String xmlText, StringBuffer estado, StringBuffer semilla )
	{
	
		try
		{			
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder     = fact.newDocumentBuilder();

			//Parse the document
			Reader reader = new CharArrayReader( xmlText.toCharArray() );
			Document doc  = builder.parse( new org.xml.sax.InputSource(reader) ); 
			
			NodeList list = doc.getElementsByTagName("SEMILLA");
	        Node node = list.item(0); 
	        semilla.append( node.getTextContent() );
	        
	        list = doc.getElementsByTagName("ESTADO");
	        node = list.item(0); 
	        estado.append( node.getTextContent() );
		}
		catch(Exception e)
		{
			return false;
		}	

		return true;
	}
	public static String getXmlToken( String semilla )
	{
		//Create instance of DocumentBuilderFactory
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//Get the DocumentBuilder
		DocumentBuilder parser;
		Document doc;
		
		try 
		{
			parser = factory.newDocumentBuilder();
			//Create blank DOM Document
			doc = parser.newDocument();

			//create the root element
			Element root = doc.createElement("getToken");
			doc.appendChild(root);
			
			Element element = doc.createElement( "item" );
			Node    node    = root.appendChild(element);

			element = createTextElement( doc, "Semilla", semilla );
			node.appendChild(element);
			
			
			
			TransformerFactory tranFactory = TransformerFactory.newInstance(); 
			Transformer aTransformer = tranFactory.newTransformer(); 
			
			aTransformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			aTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
			aTransformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			
		    //create string from xml tree
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            aTransformer.transform(source, result);
            String xmlString = sw.toString();

       //     System.out.println( "getXmlToken - Here's the xml:\n\n" + xmlString );
            return xmlString;        
		} 
		catch (Exception e)
		{
			
			System.out.println("Problemas getXmlToken " + e.getMessage());
			e.printStackTrace();
		}
		
		return null;
	}
	static Element createTextElement( Document doc, java.lang.String tag, java.lang.String valor )
	{
		Element element = doc.createElement( tag );
		Text    text    = doc.createTextNode( valor );
		element.appendChild(text);
		
		return element;
	}
	
	public static boolean parseXmlToken( String xmlText, StringBuffer estado, StringBuffer glosa, StringBuffer token )	{
		
			try
			{			
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder     = fact.newDocumentBuilder();
			
			//Parse the document
			Reader reader = new CharArrayReader( xmlText.toCharArray() );
			org.w3c.dom.Document doc  = builder.parse( new org.xml.sax.InputSource(reader) ); 
			
			org.w3c.dom.NodeList list = doc.getElementsByTagName("ESTADO");
			org.w3c.dom.Node node = list.item(0); 
			estado.append( node.getTextContent() );
			
			list = doc.getElementsByTagName("GLOSA");
			node = list.item(0); 
			glosa.append( node.getTextContent() );
			
			list = doc.getElementsByTagName("TOKEN");
			node = list.item(0); 
			token.append( node.getTextContent() );
			}
			catch(Exception e)
			{
				System.out.println("Problemas parseando xml token " + e.getMessage());
			return false;
			}	
			
			return true;
			}

	public static void main (String... a) throws Exception {
		ConexionSii con = new ConexionSii(); 
		con.autenticaProd(null, null); 
	}
}
