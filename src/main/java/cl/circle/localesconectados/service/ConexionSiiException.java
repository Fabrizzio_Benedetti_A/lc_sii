package cl.circle.localesconectados.service;

public class ConexionSiiException extends Exception {
	/**
	 */
	private static final long serialVersionUID = 1L;

	public ConexionSiiException() {
	}

	public ConexionSiiException(String message) {
		super(message);
	}

	public ConexionSiiException(Throwable cause) {
		super(cause);
	}

	public ConexionSiiException(String message, Throwable cause) {
		super(message, cause);
	}
}
