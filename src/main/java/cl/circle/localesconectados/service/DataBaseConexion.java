package cl.circle.localesconectados.service;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import cl.circle.localesconectados.enviosii.EnvioSII;


public class DataBaseConexion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	static private final String URL = "jdbc:sqlserver://54.245.137.107;databaseName=CIRCLE_LC_QA";
//	static private final String USER = "circle";
//	static private final String PASSWD = "cuRY$rSb6aw8";
	static private final String CLASS = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	 static Connection conn; 
			  
	public static Connection getConnection() throws SQLException {
		return new DataBaseConexion().createConnection(); 
	}
	
	private synchronized Connection createConnection() throws SQLException {
	    conn = null;
	  
	      try {
	        try {
	          Class.forName(CLASS);
	        } catch (ClassNotFoundException cnfe) {
	        
	          throw new SQLException(
	                       "No se encuentra el Driver de SQL", cnfe);
	        }
	        
	        String URL = "jdbc:sqlserver://"+EnvioSII.getIp()+";databaseName="+EnvioSII.getDataBase(); 
	        
	       // conn = DriverManager.getConnection(URL, USER, PASSWD);
	        conn = DriverManager.getConnection(URL, EnvioSII.getUsuario(), EnvioSII.getPassword());
	      } catch (SQLException sqle) {
	       System.err.println(sqle.getMessage());
	      }
	    
	    return conn;
	  }
	
	public static void destroy () throws SQLException {
		conn.close(); 
	}
		

}
