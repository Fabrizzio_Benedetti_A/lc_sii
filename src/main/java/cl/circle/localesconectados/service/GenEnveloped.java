package cl.circle.localesconectados.service;

import java.io.CharArrayReader;
import java.io.FileInputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

/**
 * This is a simple example of generating an Enveloped XML 
 * Signature using the JSR 105 API. The resulting signature will look 
 * like (key and signature values will be different):
 *
 * <pre><code>
 *<Envelope xmlns="urn:envelope">
 * <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
 *   <SignedInfo>
 *     <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n
-20010315"/>
 *     <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#dsa-sha1"/>
 *     <Reference URI="">
 *       <Transforms>
 *         <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
 *       </Transforms>
 *       <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
 *       <DigestValue>K8M/lPbKnuMDsO0Uzuj75lQtzQI=<DigestValue>
 *     </Reference>
 *   </SignedInfo>
 *   <SignatureValue>
 *     DpEylhQoiUKBoKWmYfajXO7LZxiDYgVtUtCNyTgwZgoChzorA2nhkQ==
 *   </SignatureValue>
 *   <KeyInfo>
 *     <KeyValue>
 *       <DSAKeyValue>
 *         <P>
 *           rFto8uPQM6y34FLPmDh40BLJ1rVrC8VeRquuhPZ6jYNFkQuwxnu/wCvIAMhukPBL
 *           FET8bJf/b2ef+oqxZajEb+88zlZoyG8g/wMfDBHTxz+CnowLahnCCTYBp5kt7G8q
 *           UobJuvjylwj1st7V9Lsu03iXMXtbiriUjFa5gURasN8=
 *         </P>
 *         <Q>
 *           kEjAFpCe4lcUOdwphpzf+tBaUds=
 *         </Q>
 *         <G>
 *           oe14R2OtyKx+s+60O5BRNMOYpIg2TU/f15N3bsDErKOWtKXeNK9FS7dWStreDxo2
 *           SSgOonqAd4FuJ/4uva7GgNL4ULIqY7E+mW5iwJ7n/WTELh98mEocsLXkNh24HcH4
 *           BZfSCTruuzmCyjdV1KSqX/Eux04HfCWYmdxN3SQ/qqw=
 *         </G>
 *         <Y>
 *           pA5NnZvcd574WRXuOA7ZfC/7Lqt4cB0MRLWtHubtJoVOao9ib5ry4rTk0r6ddnOv
 *           AIGKktutzK3ymvKleS3DOrwZQgJ+/BDWDW8kO9R66o6rdjiSobBi/0c2V1+dkqOg
 *           jFmKz395mvCOZGhC7fqAVhHat2EjGPMfgSZyABa7+1k=
 *         </Y>
 *       </DSAKeyValue>
 *     </KeyValue>
 *   </KeyInfo>
 * </Signature>
 *</Envelope>
 * </code></pre>
 */
public class GenEnveloped {
	
	public static String firma(String xmlText 
			, String aliasKeyStore 
			, String passProtection
			, KeyStore ks)throws Exception 

			{
		String xmlString =""; 
		// Create a DOM XMLSignatureFactory that will be used to
		// generate the enveloped signature.
		try {
		XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

		// Create a Reference to the enveloped document (in this case,
		// you are signing the whole document, so a URI of "" signifies
		// that, and also specify the SHA1 digest algorithm and
		// the ENVELOPED Transform.
		Reference ref = fac.newReference( "", fac.newDigestMethod(DigestMethod.SHA1, null),
				Collections.singletonList
				(fac.newTransform
						(Transform.ENVELOPED, (TransformParameterSpec) null)),
						null, null);

		// Create the SignedInfo.
		SignedInfo si = fac.newSignedInfo(fac.newCanonicalizationMethod
				(CanonicalizationMethod.INCLUSIVE,
						(C14NMethodParameterSpec) null),
						fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
						Collections.singletonList(ref));

		// Load the KeyStore and get the signing key and certificate.
		//		ks = KeyStore.getInstance("JKS");
		//			ks.load(new FileInputStream(dteKeyStore), dteKeyStorePass.toCharArray());
	
		KeyStore.PrivateKeyEntry keyEntry =  (KeyStore.PrivateKeyEntry) ks.getEntry
		(aliasKeyStore, new KeyStore.PasswordProtection(passProtection.toCharArray()));
		X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

		//
		// Create the KeyInfo containing the X509Data.
		//
		KeyInfoFactory kif = fac.getKeyInfoFactory();

		List<X509Certificate> x509Content = new ArrayList<X509Certificate>();
		//x509Content.add(cert.getSubjectX500Principal().getName());
		x509Content.add(cert);


		X509Data xd = kif.newX509Data(x509Content);
		KeyValue kv = kif.newKeyValue(cert.getPublicKey());

		List<Object> keyInfoContent = new ArrayList<Object>();
		keyInfoContent.add(kv);
		keyInfoContent.add(xd);
		KeyInfo ki = kif.newKeyInfo(keyInfoContent);



		//KeyInfo ki  = kif.newKeyInfo(Collections.singletonList(xd));


		// Instantiate the document to be signed.
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		//dbf.setNamespaceAware(true);
		dbf.setNamespaceAware(true);

		DocumentBuilder builder = dbf.newDocumentBuilder();

		//Document doc = dbf.newDocumentBuilder().parse
		//    (new FileInputStream("test.xml"));

		Reader reader = new CharArrayReader( xmlText.toCharArray() );
		Document doc = builder.parse(new org.xml.sax.InputSource(reader)); 

		// Create a DOMSignContext and specify the RSA PrivateKey and
		// location of the resulting XMLSignature's parent element.
		DOMSignContext dsc = new DOMSignContext
		(keyEntry.getPrivateKey(), doc.getDocumentElement());

		// Create the XMLSignature, but don't sign it yet.
		XMLSignature signature = fac.newXMLSignature(si, ki);

		// Marshal, generate, and sign the enveloped signature.
		signature.sign(dsc);

		// Output the resulting document.
		/*
				OutputStream os = new FileOutputStream("signedPurchaseOrder.xml");
				TransformerFactory tf = TransformerFactory.newInstance();
				Transformer trans = tf.newTransformer();
				trans.transform(new DOMSource(doc), new StreamResult(os));
		 */

		TransformerFactory tranFactory = TransformerFactory.newInstance(); 
		Transformer aTransformer = tranFactory.newTransformer(); 

		aTransformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		//aTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		aTransformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");

		//create string from xml tree
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		DOMSource source = new DOMSource(doc);
		aTransformer.transform(source, result);
		xmlString = sw.toString();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return xmlString;
			}

	public static String xfirmaNoEnveloped(//Connection connection,
			  String xmlText, String uri
			, String dteKeyStore	
			, String dteKeyStorePass
			, String aliasKeyStore 
			, String passProtection)throws Exception 
			{
		// Create a DOM XMLSignatureFactory that will be used to
		// generate the enveloped signature.
		XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

		// Create a Reference to the enveloped document (in this case,
		// you are signing the whole document, so a URI of "" signifies
		// that, and also specify the SHA1 digest algorithm and
		// the ENVELOPED Transform.

		/*
		Reference ref = fac.newReference( "#T00011", 
				fac.newDigestMethod(DigestMethod.SHA1, null),
		  Collections.singletonList
		   (fac.newTransform
		    (Transform.ENVELOPED, (TransformParameterSpec) null)),
		     null, null);
		 */

		Reference ref = fac.newReference( uri,
				fac.newDigestMethod(DigestMethod.SHA1, null));

		// Create the SignedInfo.
		SignedInfo si = fac.newSignedInfo(fac.newCanonicalizationMethod
				(CanonicalizationMethod.INCLUSIVE,
						(C14NMethodParameterSpec) null),
						fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
						Collections.singletonList(ref));

		// Load the KeyStore and get the signing key and certificate.
		KeyStore ks = KeyStore.getInstance("JKS");
		//		ks.load(new FileInputStream("dtekeystore.jks"), "dtepass".toCharArray());
		ks.load(new FileInputStream(dteKeyStore), dteKeyStorePass.toCharArray());
		KeyStore.PrivateKeyEntry keyEntry =  (KeyStore.PrivateKeyEntry) ks.getEntry
		(aliasKeyStore, new KeyStore.PasswordProtection(passProtection.toCharArray()));
		X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

		//
		// Create the KeyInfo containing the X509Data.
		//
		KeyInfoFactory kif = fac.getKeyInfoFactory();

		List<X509Certificate> x509Content = new ArrayList<X509Certificate>();
		//x509Content.add(cert.getSubjectX500Principal().getName());
		x509Content.add(cert);


		X509Data xd = kif.newX509Data(x509Content);
		KeyValue kv = kif.newKeyValue(cert.getPublicKey());

		List<Object> keyInfoContent = new ArrayList<Object>();
		keyInfoContent.add(kv);
		keyInfoContent.add(xd);
		KeyInfo ki = kif.newKeyInfo(keyInfoContent);



		//KeyInfo ki  = kif.newKeyInfo(Collections.singletonList(xd));


		// Instantiate the document to be signed.
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		//dbf.setNamespaceAware(true);
		dbf.setNamespaceAware(true);

		DocumentBuilder builder = dbf.newDocumentBuilder();

		//Document doc = dbf.newDocumentBuilder().parse
		//    (new FileInputStream("test.xml"));

		Reader reader = new CharArrayReader( xmlText.toCharArray() );
		Document doc = builder.parse(new org.xml.sax.InputSource(reader)); 

		// Create a DOMSignContext and specify the RSA PrivateKey and
		// location of the resulting XMLSignature's parent element.
		DOMSignContext dsc = new DOMSignContext
		(keyEntry.getPrivateKey(), doc.getDocumentElement());

		// Create the XMLSignature, but don't sign it yet.
		XMLSignature signature = fac.newXMLSignature(si, ki);

		// Marshal, generate, and sign the enveloped signature.
		signature.sign(dsc);

		// Output the resulting document.
		/*
		OutputStream os = new FileOutputStream("signedPurchaseOrder.xml");
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer trans = tf.newTransformer();
		trans.transform(new DOMSource(doc), new StreamResult(os));
		 */

		TransformerFactory tranFactory = TransformerFactory.newInstance(); 
		Transformer aTransformer = tranFactory.newTransformer(); 

		aTransformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		//aTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
		aTransformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");

		//create string from xml tree
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		DOMSource source = new DOMSource(doc);
		aTransformer.transform(source, result);
		String xmlString = sw.toString();

		return xmlString;
			}
	public  String firmaNoEnveloped(String xmlText , String reference
			, String aliasKeyStore 
			, String passProtection
			, KeyStore ks)throws Exception 
			{
		// Create a DOM XMLSignatureFactory that will be used to
		// generate the enveloped signature.
		XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

		// Create a Reference to the enveloped document (in this case,
		// you are signing the whole document, so a URI of "" signifies
		// that, and also specify the SHA1 digest algorithm and
		// the ENVELOPED Transform.

		Reference ref = fac.newReference(reference,
				fac.newDigestMethod(DigestMethod.SHA1, null));

		// Create the SignedInfo.
		SignedInfo si = fac.newSignedInfo(fac.newCanonicalizationMethod
				(CanonicalizationMethod.INCLUSIVE,
						(C14NMethodParameterSpec) null),
						fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
						Collections.singletonList(ref));

		// Load the KeyStore and get the signing key and certificate.
		KeyStore.PrivateKeyEntry keyEntry =  (KeyStore.PrivateKeyEntry) ks.getEntry
		(aliasKeyStore, new KeyStore.PasswordProtection(passProtection.toCharArray()));
		X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

		//
		// Create the KeyInfo containing the X509Data.
		//
		KeyInfoFactory kif = fac.getKeyInfoFactory();

		List<X509Certificate> x509Content = new ArrayList<X509Certificate>();
		x509Content.add(cert);

		X509Data xd = kif.newX509Data(x509Content);
		KeyValue kv = kif.newKeyValue(cert.getPublicKey());

		List<Object> keyInfoContent = new ArrayList<Object>();
		keyInfoContent.add(kv);
		keyInfoContent.add(xd);
		KeyInfo ki = kif.newKeyInfo(keyInfoContent);

		// Instantiate the document to be signed.
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		//dbf.setNamespaceAware(true);
		dbf.setNamespaceAware(true);

		DocumentBuilder builder = dbf.newDocumentBuilder();

		Reader reader = new CharArrayReader( xmlText.toCharArray() );
		Document doc = builder.parse(new org.xml.sax.InputSource(reader)); 

		// Create a DOMSignContext and specify the RSA PrivateKey and
		// location of the resulting XMLSignature's parent element.
		DOMSignContext dsc = new DOMSignContext
		(keyEntry.getPrivateKey(), doc.getDocumentElement());

		// Create the XMLSignature, but don't sign it yet.
		XMLSignature signature = fac.newXMLSignature(si, ki);

		// Marshal, generate, and sign the enveloped signature.
		signature.sign(dsc);

		// Output the resulting document.

		TransformerFactory tranFactory = TransformerFactory.newInstance(); 
		Transformer aTransformer = tranFactory.newTransformer(); 

		aTransformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

		//create string from xml tree
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		DOMSource source = new DOMSource(doc);
		aTransformer.transform(source, result);
		String xmlString = sw.toString();

		return xmlString;
			}
}
