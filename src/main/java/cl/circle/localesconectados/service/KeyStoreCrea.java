package cl.circle.localesconectados.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.util.Enumeration;

import org.bouncycastle.util.encoders.Base64;


public class KeyStoreCrea {
	
	public static void keyStoreCrea(String CertificadoAlias, String KeyStorePass,
            String RutEmpresa, String PassCertificado, String Certificado) throws Exception{

		   System.out.println("Creando JKS... ");
           byte[] Cert= Base64.decode(Certificado);
           ByteArrayOutputStream bOut=null;

            KeyStore kspkcs12 = KeyStore.getInstance("pkcs12");
            KeyStore ksjks = KeyStore.getInstance("jks");

            kspkcs12.load(fromIn(Cert), PassCertificado.toCharArray());
            ksjks.load(null,KeyStorePass.toCharArray());

            Enumeration eAliases = kspkcs12.aliases();
              while (eAliases.hasMoreElements()) {
                  String strAlias = (String)eAliases.nextElement();
                     if (kspkcs12.isKeyEntry(strAlias)) {
                        Key key = kspkcs12.getKey(strAlias, PassCertificado.toCharArray());
                        Certificate[] chain = kspkcs12.getCertificateChain(strAlias);
                        ksjks.setKeyEntry(CertificadoAlias, key, PassCertificado.toCharArray(), chain);
                       // ksjks.setKeyEntry(CertificadoAlias, key, KeyStorePass.toCharArray(), chain);
                     }
                bOut= new ByteArrayOutputStream();
                ksjks.store(bOut, KeyStorePass.toCharArray());
                String JKS = new String(Base64.encode(bOut.toByteArray()));  // .toBase64String(bOut.toByteArray());
                
             //   KeyStoreInsert.keyStoreInsert(con, RutEmpresa, JKS, KeyStorePass);
               
                bOut.close();
               System.out.println(JKS);
              }
    }
    	public static InputStream fromIn(byte[] str)
	{
		return new ByteArrayInputStream(str);
	}
    	

    	
    	public static KeyStore keyStoreTest (String Certificado, String PassCertificado, String CertificadoAlias, String KeyStorePass) throws Exception {
    		// KeyStore keyStoreTemp = KeyStore.getInstance("JKS");
    		 String JKS = "";
    	
    		KeyStore keyStore = KeyStore.getInstance("JKS");
    		try {	
    		//	File file = new File(Certificado);
    		
    		//  byte[] Cert= Base64.decode(Certificado);
              ByteArrayOutputStream bOut=null;

               KeyStore kspkcs12 = KeyStore.getInstance("pkcs12");
               KeyStore ksjks = KeyStore.getInstance("jks");

           //    kspkcs12.load(fromIn(FileUtils.readFileToByteArray(file)), PassCertificado.toCharArray());
               
               byte[] array = Files.readAllBytes(Paths.get(Certificado));
               
               kspkcs12.load(fromIn(array), PassCertificado.toCharArray());
               ksjks.load(null,KeyStorePass.toCharArray());

               Enumeration eAliases = kspkcs12.aliases();
                 while (eAliases.hasMoreElements()) {
                     String strAlias = (String)eAliases.nextElement();
                        if (kspkcs12.isKeyEntry(strAlias)) {
                           Key key = kspkcs12.getKey(strAlias, PassCertificado.toCharArray());
                           Certificate[] chain = kspkcs12.getCertificateChain(strAlias);
                           ksjks.setKeyEntry(CertificadoAlias, key, PassCertificado.toCharArray(), chain);
                          // ksjks.setKeyEntry(CertificadoAlias, key, KeyStorePass.toCharArray(), chain);
                        }
                   bOut= new ByteArrayOutputStream();
                   ksjks.store(bOut, KeyStorePass.toCharArray());
                   JKS = new String(Base64.encode(bOut.toByteArray()));  // .toBase64String(bOut.toByteArray());
                   
                //   KeyStoreInsert.keyStoreInsert(con, RutEmpresa, JKS, KeyStorePass);
                 
                   bOut.close();
                
          		//return ksjks; 
                 }
             
			    byte[] keyStoreBdDecode = Base64.decode(JKS.getBytes());
			    keyStore.load(fromIn(keyStoreBdDecode), KeyStorePass.toCharArray());
           		
           		Enumeration<String> aliases = keyStore.aliases();
			    while (aliases.hasMoreElements()) {
			       String alias = aliases.nextElement();
			        System.out.println("alias certificates :"+alias);
			    }
               
    		}
    		catch(Exception e) {
    			e.printStackTrace();
    		}
    		return keyStore;
    	}
    	
//    	public KeyStore keyStoreSII(String alias) throws Exception{
//    		
//    			//String alias= "SII_MAULLIN";
//    		
//    			KeyStore trustStore  = KeyStore.getInstance(KeyStore.getDefaultType());
//    			trustStore.load(null,alias.toCharArray());//Make an empty store
//    			byte[] Cert= Base64.decode(CERTIFICADO_SII_MAULLIN);
//    			InputStream fis = fromIn(Cert); /* insert your file path here */;
//    			BufferedInputStream bis = new BufferedInputStream(fis);
//
//    			CertificateFactory cf = CertificateFactory.getInstance("X.509");
//
//    			while (bis.available() > 0) {
//    			    Certificate cert = cf.generateCertificate(bis);
//    			    trustStore.setCertificateEntry(alias, cert);
//    			}
//    			Enumeration<String> aliases = trustStore.aliases();
//			    while (aliases.hasMoreElements()) {
//			       String alias2 = aliases.nextElement();
//			        System.out.println("alias certificates :"+alias2);
//			    }
//    		return trustStore; 
//        }
    	
//    	public static void main(String args[]) throws Exception {
//    		KeyStoreCrea ksc = new KeyStoreCrea(); 
//    		ksc.keyStoreSII("SII_MAULLIN");
//    	}
//    	
//    	
//    	private static String CERTIFICADO_SII_MAULLIN ="LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlHSmpDQ0JRNmdBd0lCQWdJUUJjQzNJTWtlMGJGRWFQVVI4QTZmQVRBTkJna3Foa2lHOXcwQkFRc0ZBREJQDQpNUXN3Q1FZRFZRUUdFd0pWVXpFVk1CTUdBMVVFQ2hNTVJHbG5hVU5sY25RZ1NXNWpNU2t3SndZRFZRUURFeUJFDQphV2RwUTJWeWRDQlVURk1nVWxOQklGTklRVEkxTmlBeU1ESXdJRU5CTVRBZUZ3MHlNVEF4TVRVd01EQXdNREJhDQpGdzB5TWpBeE1qVXlNelU1TlRsYU1GNHhDekFKQmdOVkJBWVRBa05NTVJFd0R3WURWUVFIRXdoVFlXNTBhV0ZuDQpiekVuTUNVR0ExVUVDaE1lVTJWeWRtbGphVzhnWkdVZ1NXMXdkV1Z6ZEc5eklFbHVkR1Z5Ym05ek1STXdFUVlEDQpWUVFERXdwM2N6SXVjMmxwTG1Oc01JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBDQp0T3V3NWpuQVZ0T2x2VE1qSFpkWUhNWUVRS2VwWm5IeUt6ckdyaCtUMFhIYzU5aTJhOEZwR21SbWd3cCt0VjQvDQozR3NuZmNVdGV5WlFsNk9wZGgydW5ta1pMdm50RDl6MEtOTGlEcU8xQmEvOG1hREhZc1NVdXF2MkJ5L1BmYVgrDQpwUUdpZ0NIRmVCQjZZUFpjMGRmalhNMDBzUUx6RW4wVHhsYlQ3RHFYWk9uUzIwUzgxeWtiWHE1aWU0bTNEaFBMDQpaRUoza1ZRUUJVaFp1ZGhFeHZBWW12K1dGNGJmQkRTdzZIMlZNdkhkazl1dnRpTitxd3U3SzFmZVNaZ0lYNDQxDQp6ZFdhdlhPSE9DdTRSV0wyd1lkaEpiWkY5L3JpTFNSK0xKNVBJaDJPVEcxU24vd0JDMFBqdktDYVVRS3FVc2h3DQo3U21lc040Vm9sS1YxcnBpSGdLd2x3SURBUUFCbzRJQzdUQ0NBdWt3SHdZRFZSMGpCQmd3Rm9BVXQydWk2cWlxDQpoSXg1NnJUYUQ1aXl4WlYydWZRd0hRWURWUjBPQkJZRUZFM3RpWVE4S29ZUTl6U1hTbVZPTnFoeGR3Um9NQlVHDQpBMVVkRVFRT01BeUNDbmR6TWk1emFXa3VZMnd3RGdZRFZSMFBBUUgvQkFRREFnV2dNQjBHQTFVZEpRUVdNQlFHDQpDQ3NHQVFVRkJ3TUJCZ2dyQmdFRkJRY0RBakNCaXdZRFZSMGZCSUdETUlHQU1ENmdQS0E2aGpob2RIUndPaTh2DQpZM0pzTXk1a2FXZHBZMlZ5ZEM1amIyMHZSR2xuYVVObGNuUlVURk5TVTBGVFNFRXlOVFl5TURJd1EwRXhMbU55DQpiREErb0R5Z09vWTRhSFIwY0RvdkwyTnliRFF1WkdsbmFXTmxjblF1WTI5dEwwUnBaMmxEWlhKMFZFeFRVbE5CDQpVMGhCTWpVMk1qQXlNRU5CTVM1amNtd3dQZ1lEVlIwZ0JEY3dOVEF6QmdabmdRd0JBZ0l3S1RBbkJnZ3JCZ0VGDQpCUWNDQVJZYmFIUjBjRG92TDNkM2R5NWthV2RwWTJWeWRDNWpiMjB2UTFCVE1IMEdDQ3NHQVFVRkJ3RUJCSEV3DQpiekFrQmdnckJnRUZCUWN3QVlZWWFIUjBjRG92TDI5amMzQXVaR2xuYVdObGNuUXVZMjl0TUVjR0NDc0dBUVVGDQpCekFDaGp0b2RIUndPaTh2WTJGalpYSjBjeTVrYVdkcFkyVnlkQzVqYjIwdlJHbG5hVU5sY25SVVRGTlNVMEZUDQpTRUV5TlRZeU1ESXdRMEV4TG1OeWREQU1CZ05WSFJNQkFmOEVBakFBTUlJQkJBWUtLd1lCQkFIV2VRSUVBZ1NCDQo5UVNCOGdEd0FIVUFLWG0rOEo0NU9TSHdWbk9mWTZWMzViNVhmWnhnQ3ZqNVRWMG1YQ1ZkeDRRQUFBRjNCNFY5DQpkZ0FBQkFNQVJqQkVBaUIySDNmeTR4bFZiVWJJRVl6Vm9KMkEwRmJiV0VsNk45TG4yU2VIbVlCN0t3SWdQcGtjDQpkNy9pMUJpdU9tYW92eEdjdE43NWpFZXdFaGxlbEtDSnliSFd5M29BZHdBaVJVVUhXVlVrVnBZL29TL3g5MjJHDQo0Q01tWTYzQVMzOWR4b05jYnVJUEFnQUFBWGNIaFgzT0FBQUVBd0JJTUVZQ0lRRFh0N0NHK0g5OG1MaklVOU1EDQo5Z01vRTQ2OHg1Nm9Jd1lSeE5DdmRHYXJGd0loQUpvQ0JaZ2d0bHp3dDNKamxsZ1kwMko1aG94eDNrUHZnRDA4DQpURW9sejdodU1BMEdDU3FHU0liM0RRRUJDd1VBQTRJQkFRQ1Y3MHFjZUhNZ3RFbEp4VEJ6VTg3Wjc5MGFvVGtGDQplZDJoRXlFTGs4RmlzdFhUa2Z0RlRRSjU0S3Y2clNkRDJmNkd5R0tIZnZ5SE82Ykl3d2NlOWFTQ2xIaGl3WjIwDQpYOThOWlFlTkVFdzdEdExkUGRGbjFBaXlNenNhMGJpd3BLMFMzcG5pWW00b1JEdVlJSFhuMjc0SUJSUFRMM2d1DQpZZm8zNGtoVHIxTjNFa0YzaHB2eTBvY25mb0hXaTU0bFNoKzl5YlpIQzZqNVIwY1lScC9xNTdFdDFMUy9Bc0UxDQpEM0VBRXdEV0NtbHdhcUxUajRyeUxwWHJoNEZ2dk1iamhDMUZ0MC9sM0txS3pyN29RdWZVcUdBZWtnYlBxMVRSDQoxLysvZmRlQldLWmdmakYzOE1SVG5paUVCMTNrWWJhMkRuMVg2aHVZOFBiRlovZVNZcjM4QmJ4NA0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQ=="; 
}
