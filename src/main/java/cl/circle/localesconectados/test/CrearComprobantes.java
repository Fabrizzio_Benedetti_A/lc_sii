package cl.circle.localesconectados.test;

import java.math.BigDecimal;
import cl.circle.localesconectados.test.sql.BalancePayUpdateCaller;
import cl.circle.localesconectados.test.sql.BalancePayUpdateResult;
import cl.circle.localesconectados.test.sql.MovementCobroDataResult;
import cl.circle.localesconectados.test.sql.MovementDataCobro2Caller;
import cl.circle.localesconectados.test.sql.MovementResult;
import cl.circle.localesconectados.test.sql.RegistraCobroComercioCaller;
import cl.circle.localesconectados.test.sql.RegistraCobroComercioResult;
import cl.circle.localesconectados.test.sql.RegistraComprobanteCaller;
import cl.circle.localesconectados.test.sql.RegistraComprobanteResult;
import cl.circle.localesconectados.test.sql.RegistraMovimientoComercioCaller;
import cl.circle.localesconectados.test.sql.RegistraMovimientoComercioResult;
import cl.circle.localesconectados.test.sql.TipoDocumentoComercioCaller;
import cl.circle.localesconectados.test.sql.TipoDocumentoComercioResult;
import cl.circle.localesconectados.test.sql.UserBeneficiaryDataCaller;
import cl.circle.localesconectados.test.sql.UserBeneficiaryDataResult;
import cl.circle.localesconectados.test.sql.UserDataCaller;
import cl.circle.localesconectados.test.sql.UserDataResult;
import cl.circle.localesconectados.test.sql.ValidateTrxResult;


/*
import cl.katarzzis.utiles.mail.EnvioMail;
import cl.katarzzis.utiles.sql.ParametrosSelectCaller;
import cl.katarzzis.utiles.sql.ParametrosSelectResult;
*/

public class CrearComprobantes {

	
	   public MovementResult createMovement(String token, String rut, BigDecimal monto, String sucursalCodigo,
	            String comentario, BigDecimal latitud, BigDecimal longitud, String formaPago){
	        MovementResult result = new MovementResult();
	        try{
	            //verifico los datos del usuario que solicita el pago
	            UserDataResult userData = UserDataCaller.execute(DataBaseConexionTest.getConnection(), token);
	            if (userData.getResultCode().intValue() != 0) {
	                result.setResultCode(userData.getResultCode());
	                result.setResultData(userData.getResultData());
	                result.setReturnValue(userData.getReturnValue());
	                return result;
	            }
	            //verifico que beneficiario exista y este activo
	            UserBeneficiaryDataResult resultBeneficiary = UserBeneficiaryDataCaller.execute(DataBaseConexionTest.getConnection(), token, rut);
	            if (resultBeneficiary.getResultCode().intValue() != 0) {
	                result.setResultCode(resultBeneficiary.getResultCode());
	                result.setResultData(resultBeneficiary.getResultData());
	                result.setReturnValue(resultBeneficiary.getReturnValue());

	                return result;
	            }
	            //verifico que tenga saldo
	            if (resultBeneficiary.getBalance()==null || resultBeneficiary.getBalance().intValue()==0){
	                result.setResultCode(new BigDecimal(-7));
	                result.setResultData("Sin saldo");
	                return result;
	            }

	            //saldo insuficiente
	            if(resultBeneficiary.getBalance().compareTo(monto)<0){
	                result.setResultCode(new BigDecimal(-5));
	                result.setResultData("Insuficiente");
	                return result;
	            }

	            RegistraCobroComercioResult resultado = RegistraCobroComercioCaller.execute(
	            		DataBaseConexionTest.getConnection(),
	                    token,
	                    resultBeneficiary.getIdUser(),
	                    sucursalCodigo,
	                    monto,
	                    comentario,
	                    latitud,
	                    longitud,
	                    formaPago
	            );
	            if (resultado.getResultCode().intValue() != 0){
	                result.setResultCode(resultado.getResultCode());
	                result.setResultData(resultado.getResultData());
	                return result;
	            }

	            result.setResultCode(resultado.getResultCode());
	            result.setResultData(resultado.getResultData());
	            result.setR001(resultado.getCodigoTrx());

	        }catch (Throwable thrown){
	            result = new MovementResult();
	            result.setResultCode(new BigDecimal(-1));
	            result.setResultData(thrown.getMessage());
	        }
	        return result;
	    }

	//como yo lo veo hay que poner todo esto en createMovementWithOutSms 
	    public ValidateTrxResult validateTransaction(String pipolToken, String trxCode, BigDecimal latitud, BigDecimal longitud) throws Exception {
	        final ValidateTrxResult result = new ValidateTrxResult();
	        try {
	            //obtener datos de la trx
	            MovementCobroDataResult resultMovementData = MovementDataCobro2Caller.execute(DataBaseConexionTest.getConnection(), pipolToken, trxCode);

	            if (resultMovementData.getResultCode().intValue() != 0) {
	                result.setReturnValue(resultMovementData.getReturnValue());
	                result.setResultCode(resultMovementData.getResultCode());
	                result.setResultData(resultMovementData.getResultData());
	                return result;
	            }

	            //consulto datos y saldo usuario
	            UserBeneficiaryDataResult userData = UserBeneficiaryDataCaller.execute(DataBaseConexionTest.getConnection(), resultMovementData.getUsuaId(), resultMovementData.getAmount());
	            if (userData.getResultCode().intValue() != 0) {
	                result.setReturnValue(userData.getReturnValue());
	                result.setResultCode(userData.getResultCode());
	                result.setResultData(userData.getResultData());
	                return result;
	            }
	    
	            //tendria que poner todo en un mismo acto ....
	            //crear movimiento
	            //registrar movimiento de comercio
	            //cargar en una billetera lo que se la ha pagado al comercio
	            //cargar saldo al comercio
	            RegistraMovimientoComercioResult resultMov = RegistraMovimientoComercioCaller.execute(DataBaseConexionTest.getConnection(), trxCode, latitud, longitud);
	            if (resultMov.getResultCode().intValue()!= 0){
	                result.setResultCode(resultMov.getResultCode());
	                result.setResultData(resultMov.getResultData());
	                return result;
	            }
	            //descontar saldo de tabla general...
	            BigDecimal saldoActual = userData.getBalance().add(resultMovementData.getAmount().negate());
	            BalancePayUpdateResult payUpdate = BalancePayUpdateCaller.execute(DataBaseConexionTest.getConnection(), resultMovementData.getUsuaId(),
	                    saldoActual, userData.getBalance(), resultMovementData.getAmount(), trxCode);

	            if (payUpdate.getResultCode().intValue()!= 0){
	                result.setResultCode(payUpdate.getResultCode());
	                result.setResultData(payUpdate.getResultData());
	                return result;
	            }
	           
				//se ha realizado un pago por @monto en @comercio, su nuevo saldo es @nuevoSaldo
			
				result.setR001(resultMov.getR001());//fechahora

	       
	          	//verificar que tipo de documento hay que enviar y donde ....
				TipoDocumentoComercioResult tdcr = TipoDocumentoComercioCaller.execute(DataBaseConexionTest.getConnection(), resultMovementData.getCommerceCode()); 
				//si es BE envio de boleta electronica 
				
				if (tdcr.getResultCode().intValue()==0)
				{
					if (tdcr.getTipoDocumento().equals("BOLETA"))
					{
//						RespuestaEnvioDocumento respuestaEnvio = this.envioBoletaElectronica(dataSource, resultMovementData.getCommerceCode(), userData.getR009(), resultMovementData.getAmount(), resultMov.getR000());
//						
//						result.setR003(respuestaEnvio.getFolio());	
//						result.setR004(respuestaEnvio.getTipo()); //en duro mientras se desarrolla el proximo
//						result.setR005(respuestaEnvio.getUrlDocumento());
					}
					if (tdcr.getTipoDocumento().equals("COMPROBANTE"))
					{
						//ACA SE GENERA EL PDF PARA SER DEVUELTO AL USUARIO Y AL COMERCIO 
	                    PDFGenerator generator = new PDFGenerator();
	                    PDFResponse pdf = generator.savePdf(DataBaseConexionTest.getConnection(), resultMovementData.getCommerceCode(), userData.getR009(), resultMovementData);
						//los comprobantes se envian desde las 00:00 asi que se registra en una tabla para saber cuales enviar. 
							//cambio estado de la transaccion  
						RegistraComprobanteResult registraComprobante = RegistraComprobanteCaller.execute(DataBaseConexionTest.getConnection(), resultMov.getR000(),  pdf.getArchivoUrl());
					
						result.setR003("0");	//folio no tiene
						result.setR004("LC"); //en duro mientras se desarrolla el proximo
						result.setR005("");
						
					}
				}
	    
				result.setR002(resultMovementData.getAmount());
				result.setR006(trxCode);
	        }
	        catch (Exception e) {
	            System.out.println(e.getMessage());
	            result.setResultCode(BigDecimal.TEN.negate());
	            result.setResultData("ERROR_10");

	        }
	        return result;
	    }

		
	  
	  public static void main (String args[]) throws Exception {
		  CrearComprobantes cc = new CrearComprobantes(); 
		  
		  
          String token= "FFFFFFFFFF";
          String run="16210500-0";
          BigDecimal monto=new BigDecimal(120);
          String sucursalCodigo="04F80629-3AE5-40DF-9FDB-C77C1BE0810F";
          String comentario="prueba cobro masivo ";
          BigDecimal latitud=BigDecimal.TEN;
          BigDecimal longitud=BigDecimal.TEN;
          String formaPago= "LC"; 
          
          for (int n= 1 ; n<101; n++) {
			MovementResult movement =  cc.createMovement(token, run, monto,sucursalCodigo, comentario+ n, latitud, longitud, formaPago);
			if (movement.getResultCode().intValue()!=0)
			{
				System.out.println(movement.getR001()); 
				System.exit(1);
			}
			cc.validateTransaction(token, movement.getR001(), latitud, longitud);
			System.out.println(n); 
          }
	  }
}
