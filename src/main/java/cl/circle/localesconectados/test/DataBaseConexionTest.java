package cl.circle.localesconectados.test;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class DataBaseConexionTest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	static private final String URL = "jdbc:sqlserver://54.245.137.107;databaseName=CIRCLE_LC_MIGRA";
	static private final String USER = "circle";
	static private final String PASSWD = "cuRY$rSb6aw8";
	static private final String CLASS = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	// static Connection conn; 
			  
	public static Connection getConnection() throws Exception {
		return new DataBaseConexionTest().createConnection(); 
	}
	
	private synchronized Connection createConnection() throws Exception {
		Connection conn = null;
	  
	      try {
	        try {
	          Class.forName(CLASS);
	        } catch (ClassNotFoundException cnfe) {
	        
	          throw new Exception(
	                       "No se encuentra el Driver de SQL", cnfe);
	        }
	        
	        String URL = "jdbc:sqlserver://"+"54.245.137.107"+";databaseName="+"CIRCLE_LC_MIGRA"; 
	        
	        
	        conn = DriverManager.getConnection(URL, USER, PASSWD);
	       // conn = DriverManager.getConnection(URL, EnvioSII.getUsuario(), EnvioSII.getPassword());
	      } catch (SQLException sqle) {
	       System.err.println(sqle.getMessage());
	      }
	    
	    return conn;
	  }
	

		

}
