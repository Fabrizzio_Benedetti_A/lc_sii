package cl.circle.localesconectados.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import cl.circle.localesconectados.test.sql.BeneficiaryDataCaller;
import cl.circle.localesconectados.test.sql.BeneficiaryDataResult;
import cl.circle.localesconectados.test.sql.CommerceDataCaller;
import cl.circle.localesconectados.test.sql.CommerceDataResult;
import cl.circle.localesconectados.test.sql.MovementCobroDataResult;
import cl.circle.localesconectados.test.sql.ParametrosSelectCaller;
import cl.circle.localesconectados.test.sql.ParametrosSelectResult;


public class PDFGenerator implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
    * Metodo para generar y guardar el pdf, devuelve la respuesta con la data del hash
    */
    //DataSource dataSource, String sucuCodigo, String beneficiarioRun, BigDecimal amount, String details,
    public PDFResponse savePdf(Connection dataSource, String sucuCodigo, String beneficiarioRun, MovementCobroDataResult movement) throws IOException, ParseException, SQLException {
        PDFResponse pdfResponse = new PDFResponse();
        CommerceDataResult commerce = CommerceDataCaller.execute(dataSource, sucuCodigo);
        BeneficiaryDataResult beneficiary = BeneficiaryDataCaller.execute(dataSource, beneficiarioRun);

        
        ParametrosSelectResult paraValor = ParametrosSelectCaller.execute(dataSource, "URL_GENERATE_PDF");
        String urlEnvio = paraValor.getParaValor();
        
       // String urlEnvio = "https://api.qa.localesconectados.cl/web/generate-transaction-pdf";

        HttpPost request = new HttpPost(urlEnvio);

        request.setHeader("Accept-Type", "application/json");
        request.setHeader("Content-Type", "application/json");

        StringEntity parametros = this.makeRequestBody(movement, commerce, beneficiary);

        request.setEntity(parametros);
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(request);

        JSONObject jsonObject = this.handleResponse(response);
        pdfResponse.setArchivoUrl((String) jsonObject.get("url"));
        pdfResponse.setArchivoExtension((String) jsonObject.get("extension"));
        pdfResponse.setArchivoMD5((String) jsonObject.get("md5"));
        pdfResponse.setArchivoNombre((String) jsonObject.get("name"));
        pdfResponse.setArchivoHash(0);
        return pdfResponse;
    }

   
    private JSONObject handleResponse(HttpResponse response) throws IOException, ParseException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader((response.getEntity().getContent()))
        );
        String line = null;
        String message = new String();
        while ((line = br.readLine()) != null) {
            message += line;
        }
        JSONParser jsonParser = new JSONParser();
        return (JSONObject) jsonParser.parse(message);
    }
    @SuppressWarnings("unchecked")
    private StringEntity makeRequestBody(MovementCobroDataResult movement, CommerceDataResult commerce, BeneficiaryDataResult beneficiary){
        JSONObject requestObject = new JSONObject();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = format.format( new Date()   );
        requestObject.put("emisorRut", commerce.getRut());
        requestObject.put("socialReason", commerce.getNombre().toString());
        requestObject.put("address", commerce.getDireccion().toString());
        requestObject.put("total", movement.getAmount());
        requestObject.put("iva", "");
        requestObject.put("nrBoleta",movement.getPaymentId());
        requestObject.put("date", dateString);
        String jsonStringData = requestObject.toJSONString();
        return new StringEntity(jsonStringData.toString(), "UTF8");
    }
}
