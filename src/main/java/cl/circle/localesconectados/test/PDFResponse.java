package cl.circle.localesconectados.test;

import java.io.Serializable;

public class PDFResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String error;
    private String archivoNombre;
    private String archivoExtension;
    private String archivoUrl;
    private String archivoMD5;
    private int archivoHash;

    /**
     * @return the archivoNombre
     */
    public String getArchivoNombre() {
        return archivoNombre;
    }
    /**
     * @param archivoNombre the archivoNombre to set
     */
    public void setArchivoNombre(String archivoNombre) {
        this.archivoNombre = archivoNombre;
    }
    /**
     * @return the archivoExtension
     */
    public String getArchivoExtension() {
        return archivoExtension;
    }
    /**
     * @param archivoExtension the archivoExtension to set
     */
    public void setArchivoExtension(String archivoExtension) {
        this.archivoExtension = archivoExtension;
    }
    /**
     * @return the archivoUrl
     */
    public String getArchivoUrl() {
        return archivoUrl;
    }
    /**
     * @param url the archivoUrl to set
     */
    public void setArchivoUrl(String url) {
        this.archivoUrl = url;
    }
    /**
     * @return the archivoMD5
     */
    public String getArchivoMD5() {
        return archivoMD5;
    }
    /**
     * @param archivoMD5 the archivoMD5 to set
     */
    public void setArchivoMD5(String archivoMD5) {
        this.archivoMD5 = archivoMD5;
    }
    /**
     * @return the archivoHash
     */
    public int getArchivoHash() {
        return archivoHash;
    }
    /**
     * @param i the archivoHash to set
     */
    public void setArchivoHash(int archivoHash) {
        this.archivoHash = archivoHash;
    }
    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }


    @Override
    public String toString() {
        return "LCResponseS3 [archivoNombre=" + archivoNombre + ", archivoExtension=" + archivoExtension
                + ", archivoUrl=" + archivoUrl + ", archivoMD5=" + archivoMD5 + ", archivoHash=" + archivoHash + "]";
    }
    /**
     * @return the error
     */
    public String getError() {
        return error;
    }
    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }
}
