package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;
import cl.circle.localesconectados.enviosii.sql.*;


public class BalancePayUpdateCaller extends ProcedureCaller
{
    /**
     * Constructs a new instance.
     */
    public BalancePayUpdateCaller()
    {
    }
    
    public static BalancePayUpdateResult execute(DataSource dataSource, BigDecimal ususId, BigDecimal saldActual, BigDecimal saldAnterior, BigDecimal saldCargo, String comentario)
            throws SQLException
        {
            return new BalancePayUpdateCaller().executeProc(dataSource, ususId, saldActual, saldAnterior, saldCargo, comentario);
        }
    public static BalancePayUpdateResult execute(Connection dataSource, BigDecimal ususId, BigDecimal saldActual, BigDecimal saldAnterior, BigDecimal saldCargo, String comentario)
            throws SQLException
        {
            return new BalancePayUpdateCaller().executeProc(dataSource, ususId, saldActual, saldAnterior, saldCargo, comentario);
        }
    
    public BalancePayUpdateResult executeProc(DataSource dataSource, BigDecimal ususId, BigDecimal saldActual, BigDecimal saldAnterior, BigDecimal saldCargo, String comentario)
            throws SQLException
        {
            final Connection conn = dataSource.getConnection();
            try {
                return executeProc(conn, ususId, saldActual, saldAnterior, saldCargo, comentario);
            } finally {
                conn.close();
            }
        }
    
    public BalancePayUpdateResult executeProc(Connection conn, BigDecimal ususId, BigDecimal saldActual, BigDecimal saldAnterior, BigDecimal saldCargo, String comentario)
            throws SQLException
        {
            final BalancePayUpdateResult result = createProcResult();
            final CallableStatement call = prepareCall(conn, "{?=call sistema.sp_Saldo_Update(?,?,?,?,?,?,?)}");
            try {
                call.registerOutParameter(1, Types.INTEGER);
                call.setBigDecimal(2, ususId);
                call.setBigDecimal(3, saldActual);
                call.setBigDecimal(4, saldAnterior);
                call.setBigDecimal(5, saldCargo);
                call.setString(6, comentario);
                call.registerOutParameter(7, Types.NUMERIC);
                call.registerOutParameter(8, Types.NVARCHAR);
                int updateCount = 0;
                boolean haveRset = call.execute();
                while (haveRset || updateCount != -1) {
                    if (!haveRset) {
                        updateCount = call.getUpdateCount();
                    } else {
                        unexpectedResultSet(call.getResultSet());
                    }
                    haveRset = call.getMoreResults();
                }
                result.setReturnValue(call.getInt(1));
                result.setResultCode(call.getBigDecimal(7));
                result.setResultData(call.getString(8));
            } finally {
                call.close();
            }
            return result;
        }
    
    protected BalancePayUpdateResult createProcResult()
    {
        return new BalancePayUpdateResult();
    }
}
