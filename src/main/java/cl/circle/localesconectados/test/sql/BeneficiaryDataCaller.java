package cl.circle.localesconectados.test.sql;

import java.sql.CallableStatement;
import cl.circle.localesconectados.enviosii.sql.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;


public class BeneficiaryDataCaller extends ProcedureCaller {

	public static BeneficiaryDataResult execute(DataSource dataSource, String beneficiarioRun) throws SQLException {
		return new BeneficiaryDataCaller().executeProc(dataSource, beneficiarioRun);
	}

	public static BeneficiaryDataResult execute(Connection dataSource, String beneficiarioRun) throws SQLException {
		return new BeneficiaryDataCaller().executeProc(dataSource, beneficiarioRun);
	}
	private BeneficiaryDataResult executeProc(DataSource dataSource, String beneficiarioRun) throws SQLException {
		Connection conn = dataSource.getConnection(); 
		try {
			return executeProc(conn, beneficiarioRun) ;
		}
		finally {
			conn.close(); 
		}
	}

	private BeneficiaryDataResult executeProc(Connection conn, String beneficiarioRun) throws SQLException {
		final BeneficiaryDataResult result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call dte.sp_Beneficiario_Select(?,?,?,?,?,?,?,?)}") ;
		try {
			call.registerOutParameter(1, Types.INTEGER);
			call.setString(2,beneficiarioRun);
			call.registerOutParameter(3, Types.NVARCHAR); //nombre
			call.registerOutParameter(4, Types.LONGNVARCHAR);
			call.registerOutParameter(5, Types.NUMERIC);
			call.registerOutParameter(6, Types.NVARCHAR);
			call.registerOutParameter(7, Types.NVARCHAR);
			call.registerOutParameter(8, Types.NUMERIC);
			call.registerOutParameter(9, Types.NVARCHAR);
			
			call.execute(); 
			
			result.setReturnValue(call.getInt(1));
			result.setNombre(call.getString(3));
			result.setEmail(call.getString(4));
			result.setTelefono(call.getBigDecimal(5));
			result.setDireccion(call.getString(6));
			result.setComuna(call.getString(7));
			result.setResultCode(call.getBigDecimal(8));
			result.setResultData(call.getString(9));
			//para ser llamado despues 
			result.setRut(beneficiarioRun);
			
		}finally {
			call.close();
		}
		return result;
	}

	private BeneficiaryDataResult createProcResult() {
		return new BeneficiaryDataResult(); 
	}

}
