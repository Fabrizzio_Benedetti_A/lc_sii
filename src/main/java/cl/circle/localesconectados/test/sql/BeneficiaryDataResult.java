package cl.circle.localesconectados.test.sql;


import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

import cl.circle.localesconectados.enviosii.sql.*;

@XmlType(name ="BeneficiaryDataResult")
public class BeneficiaryDataResult extends ProcedureResult {

	private static final long serialVersionUID = 1L;
	private int returnValue;
	private String resultData;
	private BigDecimal resultCode;

	private String nombre; 
	private String direccion; 
	private String comuna;
	private String rut; 
	private BigDecimal telefono; 
	private String email;
	
	
	public int getReturnValue() {
		return returnValue;
	}
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	public String getResultData() {
		return resultData;
	}
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	public BigDecimal getResultCode() {
		return resultCode;
	}
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public BigDecimal getTelefono() {
		return telefono;
	}
	public void setTelefono(BigDecimal telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "BeneficiaryDataResult [returnValue=" + returnValue + ", resultData=" + resultData + ", resultCode="
				+ resultCode + ", nombre=" + nombre + ", direccion=" + direccion + ", comuna=" + comuna + ", rut=" + rut
				+ ", telefono=" + telefono + ", email=" + email + "]";
	} 
	
	
	
}
