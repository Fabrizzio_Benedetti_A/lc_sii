package cl.circle.localesconectados.test.sql;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import cl.circle.localesconectados.enviosii.sql.*;

public class CommerceDataCaller extends ProcedureCaller {

	public static CommerceDataResult execute(DataSource dataSource, String sucuCodigo) throws SQLException {
		return new CommerceDataCaller().executeProc(dataSource, sucuCodigo); 
	}

	public static CommerceDataResult execute(Connection dataSource, String sucuCodigo) throws SQLException {
		return new CommerceDataCaller().executeProc(dataSource, sucuCodigo); 
	}
	private CommerceDataResult executeProc(DataSource dataSource, String sucuCodigo) throws SQLException {
		Connection conn = dataSource.getConnection(); 
		try {
			return executeProc (conn, sucuCodigo) ;
		}
		finally {
			conn.close();
		}
	}

	private CommerceDataResult executeProc(Connection conn, String sucuCodigo) throws SQLException {
		final CommerceDataResult result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call dte.sp_Comercio_Select(?,?,?,?,?,?,?,?,?,?,?)}") ;
		try {
			call.registerOutParameter(1, Types.INTEGER);
			call.setString(2,sucuCodigo);
			call.registerOutParameter(3, Types.NUMERIC);
			call.registerOutParameter(4, Types.NVARCHAR);
			call.registerOutParameter(5, Types.NVARCHAR);
			call.registerOutParameter(6, Types.NVARCHAR);
			call.registerOutParameter(7, Types.NVARCHAR);
			call.registerOutParameter(8, Types.NVARCHAR);
			call.registerOutParameter(9, Types.NVARCHAR);
			call.registerOutParameter(10, Types.NVARCHAR);
			call.registerOutParameter(11, Types.NUMERIC);
			call.registerOutParameter(12, Types.NVARCHAR);
		
			call.execute(); 
			
			result.setReturnValue(call.getInt(1));
			
			result.setAmbiente(call.getBigDecimal(3));
			result.setApiKey(call.getString(4));
			result.setRut(call.getString(5));
			result.setNombre(call.getString(6));
			result.setGiro(call.getString(7));
			result.setDireccion(call.getString(8));
			result.setComuna(call.getString(9));
			result.setCodigoSii(call.getString(10));
			result.setResultCode(call.getBigDecimal(11));
			result.setResultData(call.getString(12));
			
			
			}finally {
				call.close();
			}
			return result;
		
	}

	private CommerceDataResult createProcResult() {
		return new CommerceDataResult();
	}
	

}
