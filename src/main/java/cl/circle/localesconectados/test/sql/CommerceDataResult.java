package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;


import cl.circle.localesconectados.enviosii.sql.*;

public class CommerceDataResult extends ProcedureResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal ambiente; 
	private String apiKey; 
	private String rut;
	private String nombre;
	private String giro;
	private String direccion; 
	private String comuna;
	private String codigoSii;
	private int returnValue;
	private String resultData;
	private BigDecimal resultCode;
	
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getGiro() {
		return giro;
	}
	public void setGiro(String giro) {
		this.giro = giro;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getCodigoSii() {
		return codigoSii;
	}
	public void setCodigoSii(String codigoSii) {
		this.codigoSii = codigoSii;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}	
	public int getReturnValue() {
		return returnValue;
	}
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	public String getResultData() {
		return resultData;
	}
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	public BigDecimal getResultCode() {
		return resultCode;
	}
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	public BigDecimal getAmbiente() {
		return ambiente;
	}
	public void setAmbiente(BigDecimal ambiente) {
		this.ambiente = ambiente;
	}
	@Override
	public String toString() {
		return "CommerceDataResult [ambiente=" + ambiente + ", apiKey=" + apiKey + ", rut=" + rut + ", nombre=" + nombre
				+ ", giro=" + giro + ", direccion=" + direccion + ", comuna=" + comuna + ", codigoSii=" + codigoSii
				+ ", returnValue=" + returnValue + ", resultData=" + resultData + ", resultCode=" + resultCode + "]";
	}
	
	
}
