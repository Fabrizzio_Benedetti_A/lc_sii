package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;
import cl.circle.localesconectados.enviosii.sql.*;

public class MovementCobroDataResult extends ProcedureResult{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int returnValue;
    private BigDecimal resultCode;
    private String resultData;
    
    private BigDecimal paymentId; 
    private BigDecimal usuaId; 
    private String commerceName; 
    private String commerceEmail; 
    private BigDecimal amount; 
    private String commetary; 
    
    private String commerceCode;
    
    
    
    
	/**
	 * @return the commerceCode
	 */
	public String getCommerceCode() {
		return commerceCode;
	}
	/**
	 * @param commerceCode the commerceCode to set
	 */
	public void setCommerceCode(String commerceCode) {
		this.commerceCode = commerceCode;
	}
	/**
	 * @return the paymentId
	 */
	public BigDecimal getPaymentId() {
		return paymentId;
	}
	/**
	 * @param paymentId the paymentId to set
	 */
	public void setPaymentId(BigDecimal paymentId) {
		this.paymentId = paymentId;
	}
	/**
	 * @return the usuaId
	 */
	public BigDecimal getUsuaId() {
		return usuaId;
	}
	/**
	 * @param usuaId the usuaId to set
	 */
	public void setUsuaId(BigDecimal usuaId) {
		this.usuaId = usuaId;
	}
	/**
	 * @return the commerceName
	 */
	public String getCommerceName() {
		return commerceName;
	}
	/**
	 * @param commerceName the commerceName to set
	 */
	public void setCommerceName(String commerceName) {
		this.commerceName = commerceName;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @return the commetary
	 */
	public String getCommetary() {
		return commetary;
	}
	/**
	 * @param commetary the commetary to set
	 */
	public void setCommetary(String commetary) {
		this.commetary = commetary;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the returnValue
	 */
	public int getReturnValue() {
		return returnValue;
	}
	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	/**
	 * @return the resultCode
	 */
	public BigDecimal getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	/**
	 * @return the resultData
	 */
	public String getResultData() {
		return resultData;
	}
	/**
	 * @param resultData the resultData to set
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	public String getCommerceEmail() {
		return commerceEmail;
	}
	public void setCommerceEmail(String commerceEmail) {
		this.commerceEmail = commerceEmail;
	}

}
