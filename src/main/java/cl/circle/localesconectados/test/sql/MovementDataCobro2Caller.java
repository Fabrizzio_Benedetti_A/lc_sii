package cl.circle.localesconectados.test.sql;

import cl.circle.localesconectados.enviosii.sql.*;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class MovementDataCobro2Caller extends ProcedureCaller {
    public static MovementCobroDataResult execute(DataSource dataSource, String pipolToken, String trxCode) throws SQLException {
        return new MovementDataCobro2Caller().executeProc(dataSource, pipolToken, trxCode);
    }

    public static MovementCobroDataResult execute(Connection conn, String pipolToken, String trxCode) throws SQLException {
    	return new MovementDataCobro2Caller().executeProc(conn, pipolToken, trxCode); 
    }
    
    private MovementCobroDataResult executeProc(DataSource dataSource, String pipolToken, String trxCode)  throws SQLException{
        Connection conn = dataSource.getConnection();
        try {
            return executeProc(conn, pipolToken, trxCode);
        }
        finally {
            conn.close();
        }
    }

    private MovementCobroDataResult executeProc(Connection conn, String pipolToken, String trxCode) throws SQLException {
        final MovementCobroDataResult result = createProcResult() ;
        final CallableStatement call = prepareCall(conn, "{?=call comercio.sp_Cobro_Datos_Select_2(?,?,?,?,?,?,?,?,?,?,?)}");
        try {
            call.registerOutParameter(1, Types.INTEGER);
            call.setString(2, pipolToken);
            call.setString(3, trxCode);
            call.registerOutParameter(4, Types.NUMERIC); //pagoId
            call.registerOutParameter(5, Types.NUMERIC); //usuaId
            call.registerOutParameter(6, Types.NVARCHAR);//commerceName
            call.registerOutParameter(7, Types.NVARCHAR);//email

            call.registerOutParameter(8, Types.NUMERIC); //amount
            call.registerOutParameter(9, Types.NVARCHAR); //comentary

            call.registerOutParameter(10, Types.NVARCHAR);//comercio codigo

            call.registerOutParameter(11, Types.NUMERIC); //returnCode
            call.registerOutParameter(12, Types.NVARCHAR); //returnData

            call.execute();

            result.setReturnValue(call.getInt(1));
            result.setPaymentId(call.getBigDecimal(4));
            result.setUsuaId(call.getBigDecimal(5));
            result.setCommerceName(call.getString(6));
            result.setCommerceEmail(call.getString(7));
            result.setAmount(call.getBigDecimal(8));
            result.setCommetary(call.getString(9));

            result.setCommerceCode(call.getString(10));



            result.setResultCode(call.getBigDecimal(11));
            result.setResultData(call.getString(12));

        }
        finally  {
            conn.close();
        }
        return result;
    }

    private MovementCobroDataResult createProcResult() {
        return new MovementCobroDataResult() ;
    }

	
}
