package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

import cl.circle.localesconectados.enviosii.sql.*;

@XmlType(name = "C003Result")
public class MovementResult extends ProcedureResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int returnValue;
	private BigDecimal resultCode;
	private String resultData;
	private String R001; //codigo de movimiento 
//	private String R002; // numero
	
	/**
	 * @return the returnValue
	 */
	public int getReturnValue() {
		return returnValue;
	}
	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	/**
	 * @return the resultCode
	 */
	public BigDecimal getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	/**
	 * @return the resultData
	 */
	public String getResultData() {
		return resultData;
	}
	/**
	 * @param resultData the resultData to set
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	/**
	 * @return the r001
	 */
//	public String getR001() {
//		return R001;
//	}
//	/**
//	 * @param r001 the r001 to set
//	 */
//	public void setR001(String r001) {
//		R001 = r001;
//	}
//	/**
//	 * @return the r002
//	 */
//	public String getR002() {
//		return R002;
//	}
//	/**
//	 * @param r002 the r002 to set
//	 */
//	public void setR002(String r002) {
//		R002 = r002;
//	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the r001
	 */
	public String getR001() {
		return R001;
	}
	/**
	 * @param r001 the r001 to set
	 */
	public void setR001(String r001) {
		R001 = r001;
	}
	
	
}
