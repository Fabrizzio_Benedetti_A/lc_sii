package cl.circle.localesconectados.test.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;
import cl.circle.localesconectados.enviosii.sql.*;
/**
 * Caller of procedure <code>MOBILE$PARAMETROS_SELECT</code>.
 *
 * @author Fabrizzio Benedetti A.
 */
public class ParametrosSelectCaller extends ProcedureCaller
{
    /**
     * Constructs a new instance.
     */
    public ParametrosSelectCaller()
    {
    }

    /**
     * Executes procedure <code>MOBILE$PARAMETROS_SELECT</code> using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  paraNombre <code>PARA_Nombre nvarchar(200)</code>.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public ParametrosSelectResult executeProc(DataSource dataSource, String paraNombre)
        throws SQLException
    {
        final Connection conn = dataSource.getConnection();
        try {
            return executeProc(conn, paraNombre);
        } finally {
            conn.close();
        }
    }

    /**
     * Executes procedure <code>MOBILE$PARAMETROS_SELECT</code> using a connection.
     *
     * @param  conn the database connection.
     * @param  paraNombre <code>PARA_Nombre nvarchar(200)</code>.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public ParametrosSelectResult executeProc(Connection conn, String paraNombre)
        throws SQLException
    {
        final ParametrosSelectResult result = createProcResult();
        final CallableStatement call = prepareCall(conn, "{?=call sistema.sp_Parametros_Select(?,?,?,?)}");
        try {
            call.registerOutParameter(1, Types.INTEGER);
            call.setString(2, paraNombre);
            call.registerOutParameter(3, Types.NVARCHAR);
            call.registerOutParameter(4, Types.NUMERIC);
            call.registerOutParameter(5, Types.NVARCHAR);
            int updateCount = 0;
            boolean haveRset = call.execute();
            while (haveRset || updateCount != -1) {
                if (!haveRset) {
                    updateCount = call.getUpdateCount();
                } else {
                    unexpectedResultSet(call.getResultSet());
                }
                haveRset = call.getMoreResults();
            }
            result.setReturnValue(call.getInt(1));
            result.setParaValor(call.getString(3));
            result.setResultcode(call.getBigDecimal(4));
            result.setResultdata(call.getString(5));
        } finally {
            call.close();
        }
        return result;
    }

    /**
     * Creates a new <code>ParametrosSelectResult</code> instance.
     *
     * @return a new <code>ParametrosSelectResult</code> instance.
     */
    protected ParametrosSelectResult createProcResult()
    {
        return new ParametrosSelectResult();
    }

    /**
     * Executes procedure <code>MOBILE$PARAMETROS_SELECT</code> using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  paraNombre <code>PARA_Nombre nvarchar(200)</code>.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static ParametrosSelectResult execute(DataSource dataSource, String paraNombre)
        throws SQLException
    {
        return new ParametrosSelectCaller().executeProc(dataSource, paraNombre);
    }

    /**
     * Executes procedure <code>MOBILE$PARAMETROS_SELECT</code> using a connection.
     *
     * @param  conn the database connection.
     * @param  paraNombre <code>PARA_Nombre nvarchar(200)</code>.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static ParametrosSelectResult execute(Connection conn, String paraNombre)
        throws SQLException
    {
        return new ParametrosSelectCaller().executeProc(conn, paraNombre);
    }
}
