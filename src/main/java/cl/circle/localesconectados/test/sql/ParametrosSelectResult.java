package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;
import cl.circle.localesconectados.enviosii.sql.*;
/**
 * Results of procedure <code>MOBILE$PARAMETROS_SELECT</code>.
 *
 * <code><pre>
 * RETURN_VALUE  int       Return
 * PARA_Valor    nvarchar  Output
 * ResultCode    numeric   Output
 * ResultData    nvarchar  Output
 * </pre></code>
 *
 * @author Fabrizzio Benedetti A. 
 */
@XmlType(name = "ParametrosSelectResult")
public class ParametrosSelectResult extends ProcedureResult
{
    private static final long serialVersionUID = 1L;
    private int returnValue;
    private String paraValor;
    private BigDecimal resultcode;
    private String resultdata;

    /**
     * Constructs a new instance.
     */
    public ParametrosSelectResult()
    {
    }

    /**
     * Gets the value of property <code>returnValue</code>.
     *
     * @return the current value of the property.
     */
    public int getReturnValue()
    {
        return returnValue;
    }

    /**
     * Sets the value of property <code>returnValue</code>.
     *
     * @param value the new value of the property.
     */
    public void setReturnValue(int value)
    {
        this.returnValue = value;
    }

    /**
     * Gets the value of property <code>paraValor</code>.
     *
     * @return the current value of the property.
     */
    public String getParaValor()
    {
        return paraValor;
    }

    /**
     * Sets the value of property <code>paraValor</code>.
     *
     * @param value the new value of the property.
     */
    public void setParaValor(String value)
    {
        this.paraValor = value;
    }

    /**
     * Gets the value of property <code>resultcode</code>.
     *
     * @return the current value of the property.
     */
    public BigDecimal getResultcode()
    {
        return resultcode;
    }

    /**
     * Sets the value of property <code>resultcode</code>.
     *
     * @param value the new value of the property.
     */
    public void setResultcode(BigDecimal value)
    {
        this.resultcode = value;
    }

    /**
     * Gets the value of property <code>resultdata</code>.
     *
     * @return the current value of the property.
     */
    public String getResultdata()
    {
        return resultdata;
    }

    /**
     * Sets the value of property <code>resultdata</code>.
     *
     * @param value the new value of the property.
     */
    public void setResultdata(String value)
    {
        this.resultdata = value;
    }
}
