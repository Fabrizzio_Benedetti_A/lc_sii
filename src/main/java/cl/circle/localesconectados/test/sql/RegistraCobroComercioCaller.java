package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import cl.circle.localesconectados.enviosii.sql.*;

public class RegistraCobroComercioCaller extends ProcedureCaller{

	public RegistraCobroComercioCaller(){
		
	}

	public static RegistraCobroComercioResult execute(DataSource dataSource, String pipolToken, BigDecimal idUser, 
			String sucursalCode, BigDecimal monto, String comentario , BigDecimal latitud, BigDecimal longitud, String formaPago) throws SQLException  {
		return new RegistraCobroComercioCaller().executeProc(dataSource, pipolToken, idUser, sucursalCode, monto, comentario, latitud, longitud, formaPago); 
	}

	public static RegistraCobroComercioResult execute(Connection dataSource, String pipolToken, BigDecimal idUser, 
			String sucursalCode, BigDecimal monto, String comentario , BigDecimal latitud, BigDecimal longitud, String formaPago) throws SQLException  {
		return new RegistraCobroComercioCaller().executeProc(dataSource, pipolToken, idUser, sucursalCode, monto, comentario, latitud, longitud, formaPago); 
	}

	
	private RegistraCobroComercioResult executeProc(DataSource dataSource, String pipolToken, BigDecimal idUser,String sucursalCode,
			BigDecimal monto, String comentario, BigDecimal latitud, BigDecimal longitud, String formaPago) throws SQLException {
		final Connection conn = dataSource.getConnection();
		try {
			return executeProc(conn, pipolToken, idUser, sucursalCode,  monto, comentario, latitud, longitud, formaPago); 
		}
		finally {
			conn.close();
		}
	}

	private RegistraCobroComercioResult executeProc(Connection conn, String pipolToken, BigDecimal idUser, String sucursalCode,
			BigDecimal monto, String comentario, BigDecimal latitud, BigDecimal longitud, String formaPago) throws SQLException{
		final RegistraCobroComercioResult result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call comercio.sp_Crear_Cobro_Usuario(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}"); 
		try {
			call.registerOutParameter(1, Types.INTEGER);
			call.setString(2, pipolToken);
			call.setBigDecimal(3, idUser);
			call.setString(4, sucursalCode);
			call.setBigDecimal(5, monto);
			call.setString(6, comentario);
			call.setBigDecimal(7, latitud);
			call.setBigDecimal(8, longitud);
			call.setString(9, formaPago);
			call.registerOutParameter(10, Types.NUMERIC); //codigo
			call.registerOutParameter(11, Types.NVARCHAR);//texto
			call.registerOutParameter(12, Types.NVARCHAR);//comercio nombre
			call.registerOutParameter(13, Types.NUMERIC); //sms id 
			call.registerOutParameter(14, Types.NVARCHAR); //codigo para reenvio de sms
			call.registerOutParameter(15, Types.NUMERIC);
			call.registerOutParameter(16, Types.NVARCHAR);
			
			call.execute(); 
			
			result.setReturnValue(call.getInt(1));
			result.setCodigo(call.getBigDecimal(10)); //codigo de autorizacion enviado al usuario 
			result.setTexto(call.getString(11)); // texto sms
			result.setComercio(call.getString(12)); // comercio nombre
			result.setSmsId(call.getBigDecimal(13));// sms id
			result.setCodigoTrx(call.getString(14)); // codigo
			result.setResultCode(call.getBigDecimal(15));
			result.setResultData(call.getString(16));
		}

		finally {
    		call.close();
    	}

			return result;
	}

	private RegistraCobroComercioResult createProcResult() {
		return new RegistraCobroComercioResult();
	}
}
