package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

import cl.circle.localesconectados.enviosii.sql.*;


@XmlType(name = "RegistraCobroComercioResult")
public class RegistraCobroComercioResult extends ProcedureResult{

	private static final long serialVersionUID = 1L;
	private int returnValue;
    private BigDecimal resultCode;
    private String resultData;
//    private BigDecimal moviId; 
    private BigDecimal codigo;
    private String texto; 
    private String comercio; 
    private BigDecimal smsId;
    private String codigoTrx; //codigo para reenvio y validar trx
    
	/**
	 * @return the texto
	 */
	public String getTexto() {
		return texto;
	}
	/**
	 * @param texto the texto to set
	 */
	public void setTexto(String texto) {
		this.texto = texto;
	}
	/**
	 * @return the comercio
	 */
	public String getComercio() {
		return comercio;
	}
	/**
	 * @param comercio the comercio to set
	 */
	public void setComercio(String comercio) {
		this.comercio = comercio;
	}
	/**
	 * @return the returnValue
	 */
	public int getReturnValue() {
		return returnValue;
	}
	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	/**
	 * @return the resultCode
	 */
	public BigDecimal getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	/**
	 * @return the resultData
	 */
	public String getResultData() {
		return resultData;
	}
	/**
	 * @param resultData the resultData to set
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	
	/**
	 * @return the codigo
	 */
	public BigDecimal getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(BigDecimal codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the smsId
	 */
	public BigDecimal getSmsId() {
		return smsId;
	}
	/**
	 * @param smsId the smsId to set
	 */
	public void setSmsId(BigDecimal smsId) {
		this.smsId = smsId;
	}

	/**
	 * @return the codigoTrx
	 */
	public String getCodigoTrx() {
		return codigoTrx;
	}
	/**
	 * @param codigoTrx the codigoTrx to set
	 */
	public void setCodigoTrx(String codigoTrx) {
		this.codigoTrx = codigoTrx;
	}
	
	
	
}