package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import cl.circle.localesconectados.enviosii.sql.*;

public class RegistraComprobanteCaller extends ProcedureCaller {

	public static RegistraComprobanteResult execute(DataSource dataSource, BigDecimal idMovimiento, String urlComprobante) throws SQLException {
		return new RegistraComprobanteCaller().executeProc(dataSource, idMovimiento, urlComprobante);
				
	}
	public static RegistraComprobanteResult execute(Connection dataSource, BigDecimal idMovimiento, String urlComprobante) throws SQLException {
		return new RegistraComprobanteCaller().executeProc(dataSource, idMovimiento, urlComprobante);
				
	}
	private RegistraComprobanteResult executeProc(DataSource dataSource, BigDecimal idMovimiento, String urlComprobante) throws SQLException {
		Connection conn = dataSource.getConnection();
		try {
			return executeProc(conn, idMovimiento, urlComprobante); 
		}
		finally {
			conn.close();
		}
	}

	private RegistraComprobanteResult executeProc(Connection conn, BigDecimal idMovimiento, String urlComprobante) throws SQLException {
		RegistraComprobanteResult result = createProcResult(); 
		CallableStatement call = prepareCall(conn,"{?=call sistema.sp_Registra_Comprobante(?,?,?,?)}");
		try {
			call.registerOutParameter(1, Types.INTEGER);
			call.setBigDecimal(2, idMovimiento);
			call.setNString(3, urlComprobante);
			call.registerOutParameter(4, Types.NUMERIC);
			call.registerOutParameter(5, Types.NVARCHAR);
			
			call.execute(); 
			
			result.setReturnValue(call.getInt(1));
			result.setResultCode(call.getBigDecimal(4));
			result.setResultData(call.getString(5));
			
		}
		finally {
			call.close();
		}
		return result;
	}

	private RegistraComprobanteResult createProcResult() {
		return new RegistraComprobanteResult(); 
	}

}
