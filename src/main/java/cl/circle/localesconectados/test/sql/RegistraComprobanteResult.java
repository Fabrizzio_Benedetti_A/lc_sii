package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;

import cl.circle.localesconectados.enviosii.sql.*;

public class RegistraComprobanteResult extends ProcedureResult {

	private static final long serialVersionUID = 1L;
	private int returnValue;
	private String resultData;
	private BigDecimal resultCode;
	/**
	 * @return the returnValue
	 */
	public int getReturnValue() {
		return returnValue;
	}
	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	/**
	 * @return the resultData
	 */
	public String getResultData() {
		return resultData;
	}
	/**
	 * @param resultData the resultData to set
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	/**
	 * @return the resultCode
	 */
	public BigDecimal getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
