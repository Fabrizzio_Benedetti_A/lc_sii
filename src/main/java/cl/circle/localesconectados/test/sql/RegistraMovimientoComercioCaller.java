package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import cl.circle.localesconectados.enviosii.sql.*;

public class RegistraMovimientoComercioCaller extends ProcedureCaller{

	public static RegistraMovimientoComercioResult execute(DataSource dataSource, String trxCode, BigDecimal latitud, BigDecimal longitud) throws SQLException {
		return new RegistraMovimientoComercioCaller().executeProc(dataSource, trxCode, latitud, longitud); 
	}

	public static RegistraMovimientoComercioResult execute(Connection conn, String trxCode, BigDecimal latitud, BigDecimal longitud) throws SQLException {
		return new RegistraMovimientoComercioCaller().executeProc(conn, trxCode, latitud, longitud); 
	}
	
	private RegistraMovimientoComercioResult executeProc(DataSource dataSource, String trxCode, BigDecimal latitud, BigDecimal longitud) throws SQLException {
		Connection conn = dataSource.getConnection(); 
		try {
			return executeProc (conn, trxCode, latitud, longitud); 
		}
		finally {
			conn.close();
		}
	}

	private RegistraMovimientoComercioResult executeProc(Connection conn, String trxCode, BigDecimal latitud, BigDecimal longitud) throws SQLException {
		RegistraMovimientoComercioResult result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call comercio.sp_Crear_Movimiento_Comercio(?,?,?,?,?,?,?,?)}"); 
		try { 
			call.registerOutParameter(1, Types.INTEGER);
			call.setString(2, trxCode);
			call.setBigDecimal(3, latitud);
			call.setBigDecimal(4, longitud);
			call.registerOutParameter(5, Types.NUMERIC);
			call.registerOutParameter(6, Types.TIMESTAMP);
			call.registerOutParameter(7, Types.NUMERIC );
			call.registerOutParameter(8, Types.NUMERIC); //resultCode
			call.registerOutParameter(9, Types.NVARCHAR); //result data 
			
			call.execute() ;
			
			result.setReturnValue(call.getInt(1));
			result.setR000(call.getBigDecimal(5)); //id
			result.setR001(call.getString(6));
			result.setR002(call.getBigDecimal(7));
			result.setResultCode(call.getBigDecimal(8));
			result.setResultData(call.getString(9));
		}
		finally {
			call.close();
		}
		return result; 
	}

	private RegistraMovimientoComercioResult createProcResult() {
		return new RegistraMovimientoComercioResult() ;
	}

}
