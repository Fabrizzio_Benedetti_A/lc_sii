package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

import cl.circle.localesconectados.enviosii.sql.*;

@XmlType(name = "RegistraMovimientoComercioResult")
public class RegistraMovimientoComercioResult extends ProcedureResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int returnValue;
    private BigDecimal resultCode;
    private String resultData;
    
    private BigDecimal R000; //id interno
    
    private String R001; //fecha hora
    private BigDecimal R002; //cantidad de ventas
	/**
	 * @return the returnValue
	 */
	public int getReturnValue() {
		return returnValue;
	}
	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	/**
	 * @return the resultCode
	 */
	public BigDecimal getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	/**
	 * @return the resultData
	 */
	public String getResultData() {
		return resultData;
	}
	/**
	 * @param resultData the resultData to set
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the r001
	 */
	public String getR001() {
		return R001;
	}
	/**
	 * @param r001 the r001 to set
	 */
	public void setR001(String r001) {
		R001 = r001;
	}
	public BigDecimal getR002() {
		return R002;
	}
	public void setR002(BigDecimal r002) {
		R002 = r002;
	}
	/**
	 * @return the r000
	 */
	public BigDecimal getR000() {
		return R000;
	}
	/**
	 * @param r000 the r000 to set
	 */
	public void setR000(BigDecimal r000) {
		R000 = r000;
	}
    
    
}
