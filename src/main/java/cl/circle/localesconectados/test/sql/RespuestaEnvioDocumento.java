package cl.circle.localesconectados.test.sql;

import java.io.Serializable;

public class RespuestaEnvioDocumento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String urlDocumento; 
	private String folio; //si corresponde BE
	private String tipo; 
	/**
	 * @return the urlDocumento
	 */
	public String getUrlDocumento() {
		return urlDocumento;
	}
	/**
	 * @param urlDocumento the urlDocumento to set
	 */
	public void setUrlDocumento(String urlDocumento) {
		this.urlDocumento = urlDocumento;
	}
	/**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}
	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	
}
