package cl.circle.localesconectados.test.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import cl.circle.localesconectados.enviosii.sql.*;

public class TipoDocumentoComercioCaller extends ProcedureCaller {

	public static TipoDocumentoComercioResult execute(DataSource dataSource, String commerceCode) throws SQLException {
		return new TipoDocumentoComercioCaller().executeProc(dataSource, commerceCode);
	}

	public static TipoDocumentoComercioResult execute(Connection dataSource, String commerceCode) throws SQLException {
		return new TipoDocumentoComercioCaller().executeProc(dataSource, commerceCode);
	}
	private TipoDocumentoComercioResult executeProc(DataSource dataSource, String commerceCode) throws SQLException {
		Connection conn = dataSource.getConnection();
		try {
			return executeProc(conn, commerceCode); 
		}
		finally {
			conn.close();
		}
	}

	private TipoDocumentoComercioResult executeProc(Connection conn, String commerceCode) throws SQLException {
	    final TipoDocumentoComercioResult result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call sistema.sp_TipoDocumento_Select(?,?,?,?,?)}"); 
		try {
			call.registerOutParameter(1, Types.INTEGER);
			call.setString(2, commerceCode);
			call.registerOutParameter(3, Types.NVARCHAR); //tipo 
			call.registerOutParameter(4, Types.NVARCHAR); //ambiente
			call.registerOutParameter(5, Types.NUMERIC); //resultCode
			call.registerOutParameter(6, Types.NVARCHAR); //resultData
			
			call.execute(); 
			
			result.setTipoDocumento(call.getString(3));
			result.setAmbiente(call.getString(4));
			result.setResultCode(call.getBigDecimal(5));
			result.setResultData(call.getString(6));

		}
		finally {
			call.close();
		}
		return result; 
	}

	private TipoDocumentoComercioResult createProcResult() {
		return new TipoDocumentoComercioResult(); 
	}

}
