package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import cl.circle.localesconectados.enviosii.sql.*;


public class UserBeneficiaryDataCaller extends ProcedureCaller{

	public static UserBeneficiaryDataResult execute(DataSource dataSource, String pipolToken, String rut) throws SQLException{
		return new UserBeneficiaryDataCaller().executeProc(dataSource, pipolToken, rut);
	}
	
	public static UserBeneficiaryDataResult execute(Connection dataSource, String pipolToken, String rut) throws SQLException{
		return new UserBeneficiaryDataCaller().executeProc(dataSource, pipolToken, rut);
	}
	//consulta interna de usuario y ver si alcanza el saldo 
	public static UserBeneficiaryDataResult execute(DataSource dataSource, BigDecimal idUsuario, BigDecimal monto) throws SQLException{
		return new UserBeneficiaryDataCaller().executeProc(dataSource, idUsuario, monto);
	}
	
	public static UserBeneficiaryDataResult execute(Connection dataSource, BigDecimal idUsuario, BigDecimal monto) throws SQLException{
		return new UserBeneficiaryDataCaller().executeProc(dataSource, idUsuario, monto);
	}
	
	private UserBeneficiaryDataResult executeProc(DataSource dataSource, String pipolToken, String rut) throws SQLException{
		Connection conn = dataSource.getConnection();
		try {
			return executeProc(conn, pipolToken, rut);
		}
		finally {
			conn.close();
		}
	}
	//consulta interna 
	private UserBeneficiaryDataResult executeProc(DataSource dataSource, BigDecimal idUsuario, BigDecimal monto) throws SQLException{
		Connection conn = dataSource.getConnection();
		try {
			return executeProc(conn, idUsuario, monto);
		}
		finally {
			conn.close();
		}
	}
	
	
	private UserBeneficiaryDataResult executeProc(Connection conn, String pipolToken, String rut) throws SQLException{
		final UserBeneficiaryDataResult result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call comercio.sp_Beneficiario_Select(?,?,?,?,?,?,?,?,?,?,?,?)}");
		try {
			call.registerOutParameter(1, Types.INTEGER);
	        call.setString(2, pipolToken);
	        call.setString(3, rut);
	        call.registerOutParameter(4, Types.NUMERIC); //id
	        call.registerOutParameter(5, Types.NVARCHAR);//nombre
	        call.registerOutParameter(6, Types.NVARCHAR);//apellido paterno
	        call.registerOutParameter(7, Types.NVARCHAR);//apellido materno
	        call.registerOutParameter(8, Types.NVARCHAR);//foto
	        call.registerOutParameter(9, Types.NUMERIC);//saldo
	        call.registerOutParameter(10, Types.NVARCHAR);//email
	        call.registerOutParameter(11, Types.NUMERIC);//telefono
	        call.registerOutParameter(12, Types.NUMERIC);//resultcode
	        call.registerOutParameter(13, Types.NVARCHAR);//result data
	        
	        call.execute();
	        
	        result.setReturnValue(call.getInt(1));
	        result.setIdUser(call.getBigDecimal(4));
	        result.setR001(""+result.getIdUser());
	        result.setR002(call.getString(5));
	        result.setR003(call.getString(6));
	        result.setR004(call.getString(7));
	        result.setR005(call.getString(8));
	        result.setBalance(call.getBigDecimal(9));
	        result.setR006(""+result.getBalance());
	        result.setR007(call.getString(10));
	        
	        result.setPhone(call.getBigDecimal(11)); 
	        
	        result.setR008(""+result.getPhone());
	        result.setResultCode(call.getBigDecimal(12));
	        result.setResultData(call.getString(13));
	        
		}
	    finally {
	           call.close();
	       }

     return result;
	}
	//consulta interna
	private UserBeneficiaryDataResult executeProc(Connection conn, BigDecimal idUsuario, BigDecimal monto) throws SQLException{
		final UserBeneficiaryDataResult result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call sistema.sp_Beneficiario_Saldo_Select(?,?,?,?,?,?,?,?,?,?,?)}");
		try {
			call.registerOutParameter(1, Types.INTEGER);
	        call.setBigDecimal(2, idUsuario);
	        call.setBigDecimal(3, monto);
	        call.registerOutParameter(4, Types.NVARCHAR);//nombre
	        call.registerOutParameter(5, Types.NVARCHAR);//apellido paterno
	        call.registerOutParameter(6, Types.NVARCHAR);//apellido materno
	        call.registerOutParameter(7, Types.NVARCHAR);//foto
	        call.registerOutParameter(8, Types.NUMERIC);//saldo
	        call.registerOutParameter(9, Types.NVARCHAR);//email
	        call.registerOutParameter(10, Types.NUMERIC);//telefono
	        call.registerOutParameter(11, Types.NUMERIC);//resultcode
	        call.registerOutParameter(12, Types.NVARCHAR);//result data
	        
	        call.execute();
	        
	        result.setReturnValue(call.getInt(1));
	        result.setIdUser(idUsuario);

	        result.setR002(call.getString(4));
	        result.setR003(call.getString(5));
	        result.setR004(call.getString(6));
	        result.setR005(call.getString(7));
	        result.setBalance(call.getBigDecimal(8));
	        result.setR007(call.getString(9));
	        result.setPhone(call.getBigDecimal(10)); 
	        result.setResultCode(call.getBigDecimal(11));
	        result.setResultData(call.getString(12));
	        
		}
	    finally {
	           call.close();
	       }

     return result;
	}
	
	private UserBeneficiaryDataResult createProcResult() {
		return new UserBeneficiaryDataResult();
	}

}
