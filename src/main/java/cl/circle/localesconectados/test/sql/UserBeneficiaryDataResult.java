package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

import cl.circle.localesconectados.enviosii.sql.*;

@XmlType(name="C002Result")
public class UserBeneficiaryDataResult extends ProcedureResult {

	private static final long serialVersionUID = 1L;
	private int returnValue;
    private BigDecimal resultCode;
    private String resultData;
    private String R001; //id AES
    private String R002; //Nombre
    private String R003; //Apellido paterno
    private String R004; //Apellido materno
    private String R005; //foto
    private String R006; // Saldo AES
    private String R007; //email
    private String R008; // telefono
    
   
    private String R009;//RUN
    private String R010;// comunaId
    private String R011;//comuna nombre
   
    private String R012; //tipo
    private String R013; //colectivo nombre
    private String R014; //sector
    private String R015; //contactos   
    private boolean R0016; // estado 
    
    private BigDecimal balance; //saldo
    private BigDecimal idUser; //id uuario
    private BigDecimal phone; // telefono 
    
    
    
    public void nullAll() {
    	this.setR001(null);
    	this.setR002(null);
    	this.setR003(null);
    	this.setR004(null);
    	this.setR005(null);
    	this.setR006(null);
    	this.setR007(null);
    	this.setR008(null);
    	this.setR009(null);
    	this.setR010(null);
    	this.setR011(null);
    	this.setR012(null);
    	this.setR013(null);
    	this.setR014(null);
    	this.setR015(null);
    	this.setBalance(null);
    	this.setIdUser(null);
    	this.setPhone(null);
    }

    
	/**
	 * @return the r012
	 */
	public String getR012() {
		return R012;
	}

	/**
	 * @param r012 the r012 to set
	 */
	public void setR012(String r012) {
		R012 = r012;
	}

	/**
	 * @return the r013
	 */
	public String getR013() {
		return R013;
	}

	/**
	 * @param r013 the r013 to set
	 */
	public void setR013(String r013) {
		R013 = r013;
	}

	/**
	 * @return the r014
	 */
	public String getR014() {
		return R014;
	}

	/**
	 * @param r014 the r014 to set
	 */
	public void setR014(String r014) {
		R014 = r014;
	}

	/**
	 * @return the r015
	 */
	public String getR015() {
		return R015;
	}

	/**
	 * @param r015 the r015 to set
	 */
	public void setR015(String r015) {
		R015 = r015;
	}

	/**
	 * @return the returnValue
	 */

    
	public int getReturnValue() {
		return returnValue;
	}
	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	/**
	 * @return the resultCode
	 */
	public BigDecimal getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	/**
	 * @return the resultData
	 */
	public String getResultData() {
		return resultData;
	}
	/**
	 * @param resultData the resultData to set
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	/**
	 * @return the r001
	 */
	public String getR001() {
		return R001;
	}
	/**
	 * @param r001 the r001 to set
	 */
	public void setR001(String r001) {
		R001 = r001;
	}
	/**
	 * @return the r002
	 */
	public String getR002() {
		return R002;
	}
	/**
	 * @param r002 the r002 to set
	 */
	public void setR002(String r002) {
		R002 = r002;
	}
	/**
	 * @return the r003
	 */
	public String getR003() {
		return R003;
	}
	/**
	 * @param r003 the r003 to set
	 */
	public void setR003(String r003) {
		R003 = r003;
	}
	/**
	 * @return the r004
	 */
	public String getR004() {
		return R004;
	}
	/**
	 * @param r004 the r004 to set
	 */
	public void setR004(String r004) {
		R004 = r004;
	}
	/**
	 * @return the r005
	 */
	public String getR005() {
		return R005;
	}
	/**
	 * @param r005 the r005 to set
	 */
	public void setR005(String r005) {
		R005 = r005;
	}
	/**
	 * @return the r006
	 */
	public String getR006() {
		return R006;
	}
	/**
	 * @param r006 the r006 to set
	 */
	public void setR006(String r006) {
		R006 = r006;
	}
	/**
	 * @return the r007
	 */
	public String getR007() {
		return R007;
	}
	/**
	 * @param r007 the r007 to set
	 */
	public void setR007(String r007) {
		R007 = r007;
	}
	/**
	 * @return the r008
	 */
	public String getR008() {
		return R008;
	}
	/**
	 * @param r008 the r008 to set
	 */
	public void setR008(String r008) {
		R008 = r008;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	/**
	 * @return the idUser
	 */
	public BigDecimal getIdUser() {
		return idUser;
	}
	/**
	 * @param idUser the idUser to set
	 */
	public void setIdUser(BigDecimal idUser) {
		this.idUser = idUser;
	}
	/**
	 * @return the phone
	 */
	public BigDecimal getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(BigDecimal phone) {
		this.phone = phone;
	}

	/**
	 * @return the r009
	 */
	public String getR009() {
		return R009;
	}

	/**
	 * @param r009 the r009 to set
	 */
	public void setR009(String r009) {
		R009 = r009;
	}

	/**
	 * @return the r010
	 */
	public String getR010() {
		return R010;
	}

	/**
	 * @param r010 the r010 to set
	 */
	public void setR010(String r010) {
		R010 = r010;
	}

	/**
	 * @return the r011
	 */
	public String getR011() {
		return R011;
	}

	/**
	 * @param r011 the r011 to set
	 */
	public void setR011(String r011) {
		R011 = r011;
	}


	/**
	 * @return the r0016
	 */
	public boolean isR0016() {
		return R0016;
	}


	/**
	 * @param r0016 the r0016 to set
	 */
	public void setR0016(boolean r0016) {
		R0016 = r0016;
	}
	
    
    
}
