package cl.circle.localesconectados.test.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import cl.circle.localesconectados.enviosii.sql.*;


public class UserDataCaller extends ProcedureCaller{

	public UserDataCaller() {
		
	}

	public static UserDataResult execute(DataSource dataSource, String pipolToken) throws SQLException {
		return new UserDataCaller().executeProc(dataSource, pipolToken);
	}

	public static UserDataResult execute(Connection dataSource, String pipolToken) throws SQLException {
		return new UserDataCaller().executeProc(dataSource, pipolToken);
	}
	private UserDataResult executeProc(DataSource dataSource, String pipolToken) throws SQLException{
		final Connection conn = dataSource.getConnection();
		try {
			return executeProc(conn, pipolToken);
		} finally {
			conn.close();
		}
	}

	private UserDataResult executeProc(Connection conn, String pipolToken) throws SQLException{
		final UserDataResult result = createProcResult(); 
		final CallableStatement call = prepareCall(conn, "{?=call comercio.sp_Usuario_Datos_Select(?,?,?,?,?,?,?,?)}");
		try {
			
		call.registerOutParameter(1, Types.INTEGER);
    	call.setString(2, pipolToken);
    	call.registerOutParameter(3, Types.NUMERIC);
    	call.registerOutParameter(4, Types.NVARCHAR);
    	call.registerOutParameter(5, Types.NVARCHAR);
    	call.registerOutParameter(6, Types.NVARCHAR);
    	call.registerOutParameter(7, Types.NVARCHAR);
    	
    	call.registerOutParameter(8, Types.NUMERIC);
    	call.registerOutParameter(9, Types.NVARCHAR);
    	
    	call.execute(); 
    	result.setReturnValue(call.getInt(1));
    	result.setUsuaId(call.getBigDecimal(3));
    	result.setNombre(call.getString(4));
    	result.setApellidoP(call.getString(5));
    	result.setSocket(call.getString(6));
    	result.setTokenDispositivo(call.getString(7));
    	result.setResultCode(call.getBigDecimal(8));
    	result.setResultData(call.getString(9));
	}
    	finally {
             call.close();
         }
 	
 	return result;
	}

	private UserDataResult createProcResult() {
		return new UserDataResult();
	}
	
}
