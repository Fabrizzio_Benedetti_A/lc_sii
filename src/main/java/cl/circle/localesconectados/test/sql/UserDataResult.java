package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlType;

import cl.circle.localesconectados.enviosii.sql.*;

@XmlType(name = "UserDataResult")
public class UserDataResult extends ProcedureResult{

	private static final long serialVersionUID = 1L;
	
	 private int returnValue;
	 private BigDecimal resultCode;
	 private String resultData;
	
	 private BigDecimal usuaId; 
	 private String nombre; 
	 private String apellidoP;
	 private String socket; 
	 private String tokenDispositivo;
	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	/**
	 * @return the returnValue
	 */
	public int getReturnValue() {
		return returnValue;
	}
	/**
	 * @return the resultCode
	 */
	public BigDecimal getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	/**
	 * @return the resultData
	 */
	public String getResultData() {
		return resultData;
	}
	/**
	 * @param resultData the resultData to set
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	/**
	 * @return the usuaId
	 */
	public BigDecimal getUsuaId() {
		return usuaId;
	}
	/**
	 * @param usuaId the usuaId to set
	 */
	public void setUsuaId(BigDecimal usuaId) {
		this.usuaId = usuaId;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellidoP
	 */
	public String getApellidoP() {
		return apellidoP;
	}
	/**
	 * @param apellidoP the apellidoP to set
	 */
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	/**
	 * @return the socket
	 */
	public String getSocket() {
		return socket;
	}
	/**
	 * @param socket the socket to set
	 */
	public void setSocket(String socket) {
		this.socket = socket;
	}
	/**
	 * @return the tokenDispositivo
	 */
	public String getTokenDispositivo() {
		return tokenDispositivo;
	}
	/**
	 * @param tokenDispositivo the tokenDispositivo to set
	 */
	public void setTokenDispositivo(String tokenDispositivo) {
		this.tokenDispositivo = tokenDispositivo;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	 
}
