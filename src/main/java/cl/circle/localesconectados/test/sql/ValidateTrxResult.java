package cl.circle.localesconectados.test.sql;

import java.math.BigDecimal;

import cl.circle.localesconectados.enviosii.sql.*;

public class ValidateTrxResult extends ProcedureResult{

	private static final long serialVersionUID = 1L;

	private int returnValue;
	private BigDecimal resultCode;
	private String resultData;
	private String R001; //fecha hora de la trx
	private BigDecimal R002; //total
	private String R003; // numero boleta
	private String R004; //tipo venta
	private String R005; //url boleta
	private String R006; // codigo boleta

	public String getR006() {
		return R006;
	}

	public void setR006(String r006) {
		R006 = r006;
	}

	/**
	 * @return the returnValue
	 */
	public int getReturnValue() {
		return returnValue;
	}
	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}
	/**
	 * @return the resultCode
	 */
	public BigDecimal getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(BigDecimal resultCode) {
		this.resultCode = resultCode;
	}
	/**
	 * @return the resultData
	 */
	public String getResultData() {
		return resultData;
	}
	/**
	 * @param resultData the resultData to set
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the r001
	 */
	public String getR001() {
		return R001;
	}
	/**
	 * @param r001 the r001 to set
	 */
	public void setR001(String r001) {
		R001 = r001;
	}
	/**
	 * @return the r002
	 */
	public BigDecimal getR002() {
		return R002;
	}
	/**
	 * @param r002 the r002 to set
	 */
	public void setR002(BigDecimal r002) {
		R002 = r002;
	}
	/**
	 * @return the r003
	 */
	public String getR003() {
		return R003;
	}
	/**
	 * @param r003 the r003 to set
	 */
	public void setR003(String r003) {
		R003 = r003;
	}
	/**
	 * @return the r004
	 */
	public String getR004() {
		return R004;
	}
	/**
	 * @param r004 the r004 to set
	 */
	public void setR004(String r004) {
		R004 = r004;
	}
	/**
	 * @return the r005
	 */
	public String getR005() {
		return R005;
	}
	/**
	 * @param r005 the r005 to set
	 */
	public void setR005(String r005) {
		R005 = r005;
	}
	
	
}
