/**
 * CrSeed.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.circle.sii.seed;

public interface CrSeed extends java.rmi.Remote {
    public java.lang.String getVersionMayor() throws java.rmi.RemoteException;
    public java.lang.String getVersionMenor() throws java.rmi.RemoteException;
    public java.lang.String getVersionPatch() throws java.rmi.RemoteException;
    public java.lang.String getSeed() throws java.rmi.RemoteException;
    public java.lang.String getState() throws java.rmi.RemoteException;
}
