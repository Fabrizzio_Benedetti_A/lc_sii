package cl.circle.sii.seed;

public class CrSeedProxy implements cl.circle.sii.seed.CrSeed {
  private String _endpoint = null;
  private cl.circle.sii.seed.CrSeed crSeed = null;
  
  public CrSeedProxy() {
    _initCrSeedProxy();
  }
  
  public CrSeedProxy(String endpoint) {
    _endpoint = endpoint;
    _initCrSeedProxy();
  }
  
  private void _initCrSeedProxy() {
    try {
      crSeed = (new cl.circle.sii.seed.CrSeedServiceLocator()).getCrSeed();
      if (crSeed != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)crSeed)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)crSeed)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (crSeed != null)
      ((javax.xml.rpc.Stub)crSeed)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.circle.sii.seed.CrSeed getCrSeed() {
    if (crSeed == null)
      _initCrSeedProxy();
    return crSeed;
  }
  
  public java.lang.String getVersionMayor() throws java.rmi.RemoteException{
    if (crSeed == null)
      _initCrSeedProxy();
    return crSeed.getVersionMayor();
  }
  
  public java.lang.String getVersionMenor() throws java.rmi.RemoteException{
    if (crSeed == null)
      _initCrSeedProxy();
    return crSeed.getVersionMenor();
  }
  
  public java.lang.String getVersionPatch() throws java.rmi.RemoteException{
    if (crSeed == null)
      _initCrSeedProxy();
    return crSeed.getVersionPatch();
  }
  
  public java.lang.String getSeed() throws java.rmi.RemoteException{
    if (crSeed == null)
      _initCrSeedProxy();
    return crSeed.getSeed();
  }
  
  public java.lang.String getState() throws java.rmi.RemoteException{
    if (crSeed == null)
      _initCrSeedProxy();
    return crSeed.getState();
  }
  
  
}