/**
 * CrSeedService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.circle.sii.seed;

public interface CrSeedService extends javax.xml.rpc.Service {
    public java.lang.String getCrSeedAddress();

    public cl.circle.sii.seed.CrSeed getCrSeed() throws javax.xml.rpc.ServiceException;

    public cl.circle.sii.seed.CrSeed getCrSeed(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
