/**
 * CrSeedServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.circle.sii.seed;

public class CrSeedServiceLocator extends org.apache.axis.client.Service implements cl.circle.sii.seed.CrSeedService {

    public CrSeedServiceLocator() {
    }


    public CrSeedServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CrSeedServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CrSeed
    private java.lang.String CrSeed_address = "https://palena.sii.cl/DTEWS/CrSeed.jws";

    public java.lang.String getCrSeedAddress() {
        return CrSeed_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CrSeedWSDDServiceName = "CrSeed";

    public java.lang.String getCrSeedWSDDServiceName() {
        return CrSeedWSDDServiceName;
    }

    public void setCrSeedWSDDServiceName(java.lang.String name) {
        CrSeedWSDDServiceName = name;
    }

    public cl.circle.sii.seed.CrSeed getCrSeed() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CrSeed_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCrSeed(endpoint);
    }

    public cl.circle.sii.seed.CrSeed getCrSeed(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.circle.sii.seed.CrSeedSoapBindingStub _stub = new cl.circle.sii.seed.CrSeedSoapBindingStub(portAddress, this);
            _stub.setPortName(getCrSeedWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCrSeedEndpointAddress(java.lang.String address) {
        CrSeed_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.circle.sii.seed.CrSeed.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.circle.sii.seed.CrSeedSoapBindingStub _stub = new cl.circle.sii.seed.CrSeedSoapBindingStub(new java.net.URL(CrSeed_address), this);
                _stub.setPortName(getCrSeedWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CrSeed".equals(inputPortName)) {
            return getCrSeed();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://DefaultNamespace", "CrSeedService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://DefaultNamespace", "CrSeed"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CrSeed".equals(portName)) {
            setCrSeedEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
