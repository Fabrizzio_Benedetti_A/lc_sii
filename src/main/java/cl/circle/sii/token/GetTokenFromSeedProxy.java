package cl.circle.sii.token;

public class GetTokenFromSeedProxy implements cl.circle.sii.token.GetTokenFromSeed {
  private String _endpoint = null;
  private cl.circle.sii.token.GetTokenFromSeed getTokenFromSeed = null;
  
  public GetTokenFromSeedProxy() {
    _initGetTokenFromSeedProxy();
  }
  
  public GetTokenFromSeedProxy(String endpoint) {
    _endpoint = endpoint;
    _initGetTokenFromSeedProxy();
  }
  
  private void _initGetTokenFromSeedProxy() {
    try {
      getTokenFromSeed = (new cl.circle.sii.token.GetTokenFromSeedServiceLocator()).getGetTokenFromSeed();
      if (getTokenFromSeed != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)getTokenFromSeed)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)getTokenFromSeed)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (getTokenFromSeed != null)
      ((javax.xml.rpc.Stub)getTokenFromSeed)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cl.circle.sii.token.GetTokenFromSeed getGetTokenFromSeed() {
    if (getTokenFromSeed == null)
      _initGetTokenFromSeedProxy();
    return getTokenFromSeed;
  }
  
  public java.lang.String getVersionMayor() throws java.rmi.RemoteException{
    if (getTokenFromSeed == null)
      _initGetTokenFromSeedProxy();
    return getTokenFromSeed.getVersionMayor();
  }
  
  public java.lang.String getVersionMenor() throws java.rmi.RemoteException{
    if (getTokenFromSeed == null)
      _initGetTokenFromSeedProxy();
    return getTokenFromSeed.getVersionMenor();
  }
  
  public java.lang.String getVersionPatch() throws java.rmi.RemoteException{
    if (getTokenFromSeed == null)
      _initGetTokenFromSeedProxy();
    return getTokenFromSeed.getVersionPatch();
  }
  
  public java.lang.String getToken(java.lang.String pszXml) throws java.rmi.RemoteException{
    if (getTokenFromSeed == null)
      _initGetTokenFromSeedProxy();
    return getTokenFromSeed.getToken(pszXml);
  }
  
  public java.lang.String getState() throws java.rmi.RemoteException{
    if (getTokenFromSeed == null)
      _initGetTokenFromSeedProxy();
    return getTokenFromSeed.getState();
  }
  
  
}