/**
 * GetTokenFromSeedService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.circle.sii.token;

public interface GetTokenFromSeedService extends javax.xml.rpc.Service {
    public java.lang.String getGetTokenFromSeedAddress();

    public cl.circle.sii.token.GetTokenFromSeed getGetTokenFromSeed() throws javax.xml.rpc.ServiceException;

    public cl.circle.sii.token.GetTokenFromSeed getGetTokenFromSeed(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
