/**
 * GetTokenFromSeedServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cl.circle.sii.token;

public class GetTokenFromSeedServiceLocator extends org.apache.axis.client.Service implements cl.circle.sii.token.GetTokenFromSeedService {

    public GetTokenFromSeedServiceLocator() {
    }


    public GetTokenFromSeedServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GetTokenFromSeedServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GetTokenFromSeed
    private java.lang.String GetTokenFromSeed_address = "https://palena.sii.cl/DTEWS/GetTokenFromSeed.jws";

    public java.lang.String getGetTokenFromSeedAddress() {
        return GetTokenFromSeed_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GetTokenFromSeedWSDDServiceName = "GetTokenFromSeed";

    public java.lang.String getGetTokenFromSeedWSDDServiceName() {
        return GetTokenFromSeedWSDDServiceName;
    }

    public void setGetTokenFromSeedWSDDServiceName(java.lang.String name) {
        GetTokenFromSeedWSDDServiceName = name;
    }

    public cl.circle.sii.token.GetTokenFromSeed getGetTokenFromSeed() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GetTokenFromSeed_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGetTokenFromSeed(endpoint);
    }

    public cl.circle.sii.token.GetTokenFromSeed getGetTokenFromSeed(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cl.circle.sii.token.GetTokenFromSeedSoapBindingStub _stub = new cl.circle.sii.token.GetTokenFromSeedSoapBindingStub(portAddress, this);
            _stub.setPortName(getGetTokenFromSeedWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGetTokenFromSeedEndpointAddress(java.lang.String address) {
        GetTokenFromSeed_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cl.circle.sii.token.GetTokenFromSeed.class.isAssignableFrom(serviceEndpointInterface)) {
                cl.circle.sii.token.GetTokenFromSeedSoapBindingStub _stub = new cl.circle.sii.token.GetTokenFromSeedSoapBindingStub(new java.net.URL(GetTokenFromSeed_address), this);
                _stub.setPortName(getGetTokenFromSeedWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GetTokenFromSeed".equals(inputPortName)) {
            return getGetTokenFromSeed();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://DefaultNamespace", "GetTokenFromSeedService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://DefaultNamespace", "GetTokenFromSeed"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GetTokenFromSeed".equals(portName)) {
            setGetTokenFromSeedEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
