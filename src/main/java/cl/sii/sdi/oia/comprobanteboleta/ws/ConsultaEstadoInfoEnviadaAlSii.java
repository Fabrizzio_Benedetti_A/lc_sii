
package cl.sii.sdi.oia.comprobanteboleta.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para consultaEstadoInfoEnviadaAlSii complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="consultaEstadoInfoEnviadaAlSii">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rutInformante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dvInformante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rutContribuyente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dvContribuyente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaVenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaEstadoInfoEnviadaAlSii", propOrder = {
    "rutInformante",
    "dvInformante",
    "rutContribuyente",
    "dvContribuyente",
    "tipoDocumento",
    "fechaVenta"
})
public class ConsultaEstadoInfoEnviadaAlSii {

    protected String rutInformante;
    protected String dvInformante;
    protected String rutContribuyente;
    protected String dvContribuyente;
    protected String tipoDocumento;
    protected String fechaVenta;

    /**
     * Obtiene el valor de la propiedad rutInformante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutInformante() {
        return rutInformante;
    }

    /**
     * Define el valor de la propiedad rutInformante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutInformante(String value) {
        this.rutInformante = value;
    }

    /**
     * Obtiene el valor de la propiedad dvInformante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvInformante() {
        return dvInformante;
    }

    /**
     * Define el valor de la propiedad dvInformante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvInformante(String value) {
        this.dvInformante = value;
    }

    /**
     * Obtiene el valor de la propiedad rutContribuyente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutContribuyente() {
        return rutContribuyente;
    }

    /**
     * Define el valor de la propiedad rutContribuyente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutContribuyente(String value) {
        this.rutContribuyente = value;
    }

    /**
     * Obtiene el valor de la propiedad dvContribuyente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContribuyente() {
        return dvContribuyente;
    }

    /**
     * Define el valor de la propiedad dvContribuyente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContribuyente(String value) {
        this.dvContribuyente = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Define el valor de la propiedad tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVenta() {
        return fechaVenta;
    }

    /**
     * Define el valor de la propiedad fechaVenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVenta(String value) {
        this.fechaVenta = value;
    }

}
