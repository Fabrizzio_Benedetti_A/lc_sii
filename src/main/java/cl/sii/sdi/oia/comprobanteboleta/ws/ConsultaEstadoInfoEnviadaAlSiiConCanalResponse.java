
package cl.sii.sdi.oia.comprobanteboleta.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para consultaEstadoInfoEnviadaAlSiiConCanalResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="consultaEstadoInfoEnviadaAlSiiConCanalResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.comprobanteboleta.oia.sdi.sii.cl}transaccionRespConCanalTo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaEstadoInfoEnviadaAlSiiConCanalResponse", propOrder = {
    "_return"
})
public class ConsultaEstadoInfoEnviadaAlSiiConCanalResponse {

    @XmlElement(name = "return")
    protected TransaccionRespConCanalTo _return;

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link TransaccionRespConCanalTo }
     *     
     */
    public TransaccionRespConCanalTo getReturn() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link TransaccionRespConCanalTo }
     *     
     */
    public void setReturn(TransaccionRespConCanalTo value) {
        this._return = value;
    }

}
