
package cl.sii.sdi.oia.comprobanteboleta.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para consultaEstadoInfoPorTrackId complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="consultaEstadoInfoPorTrackId">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rutInformante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dvInformante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trackid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaEstadoInfoPorTrackId", propOrder = {
    "rutInformante",
    "dvInformante",
    "trackid"
})
public class ConsultaEstadoInfoPorTrackId {

    protected String rutInformante;
    protected String dvInformante;
    protected String trackid;

    /**
     * Obtiene el valor de la propiedad rutInformante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutInformante() {
        return rutInformante;
    }

    /**
     * Define el valor de la propiedad rutInformante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutInformante(String value) {
        this.rutInformante = value;
    }

    /**
     * Obtiene el valor de la propiedad dvInformante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvInformante() {
        return dvInformante;
    }

    /**
     * Define el valor de la propiedad dvInformante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvInformante(String value) {
        this.dvInformante = value;
    }

    /**
     * Obtiene el valor de la propiedad trackid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackid() {
        return trackid;
    }

    /**
     * Define el valor de la propiedad trackid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackid(String value) {
        this.trackid = value;
    }

}
