
package cl.sii.sdi.oia.comprobanteboleta.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para consultaEstadoInfoPorTrackIdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="consultaEstadoInfoPorTrackIdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.comprobanteboleta.oia.sdi.sii.cl}transaccionRespTo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaEstadoInfoPorTrackIdResponse", propOrder = {
    "_return"
})
public class ConsultaEstadoInfoPorTrackIdResponse {

    @XmlElement(name = "return")
    protected TransaccionRespTo _return;

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link TransaccionRespTo }
     *     
     */
    public TransaccionRespTo getReturn() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link TransaccionRespTo }
     *     
     */
    public void setReturn(TransaccionRespTo value) {
        this._return = value;
    }

}
