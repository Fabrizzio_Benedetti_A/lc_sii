
package cl.sii.sdi.oia.comprobanteboleta.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ingresarTransaccionesConCanalResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ingresarTransaccionesConCanalResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RespuestaTransaccionesTo" type="{http://ws.comprobanteboleta.oia.sdi.sii.cl}respuestaTransaccionesTo" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ingresarTransaccionesConCanalResponse", propOrder = {
    "respuestaTransaccionesTo"
})
public class IngresarTransaccionesConCanalResponse {

    @XmlElement(name = "RespuestaTransaccionesTo", namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl")
    protected RespuestaTransaccionesTo respuestaTransaccionesTo;

    /**
     * Obtiene el valor de la propiedad respuestaTransaccionesTo.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaTransaccionesTo }
     *     
     */
    public RespuestaTransaccionesTo getRespuestaTransaccionesTo() {
        return respuestaTransaccionesTo;
    }

    /**
     * Define el valor de la propiedad respuestaTransaccionesTo.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaTransaccionesTo }
     *     
     */
    public void setRespuestaTransaccionesTo(RespuestaTransaccionesTo value) {
        this.respuestaTransaccionesTo = value;
    }

}
