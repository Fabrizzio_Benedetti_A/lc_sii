
package cl.sii.sdi.oia.comprobanteboleta.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.sii.sdi.oia.comprobanteboleta.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultaEstadoInfoPorTrackIdConCanal_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaEstadoInfoPorTrackIdConCanal");
    private final static QName _IngresarTransaccion_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "ingresarTransaccion");
    private final static QName _ConsultaEstadoInfoPorTrackIdConCanalResponse_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaEstadoInfoPorTrackIdConCanalResponse");
    private final static QName _IngresarTransaccionesConCanal_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "ingresarTransaccionesConCanal");
    private final static QName _ConsultaEstadoInfoEnviadaAlSiiResponse_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaEstadoInfoEnviadaAlSiiResponse");
    private final static QName _ConsultaEstadoInfoEnviadaAlSii_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaEstadoInfoEnviadaAlSii");
    private final static QName _ConsultaEstadoInfoPorTrackIdResponse_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaEstadoInfoPorTrackIdResponse");
    private final static QName _ConsultaTransaccionesPorIdEnvioConCanal_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaTransaccionesPorIdEnvioConCanal");
    private final static QName _GetVersion_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "getVersion");
    private final static QName _ConsultaEstadoInfoPorTrackId_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaEstadoInfoPorTrackId");
    private final static QName _ConsultaTransaccionesPorIdEnvio_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaTransaccionesPorIdEnvio");
    private final static QName _ConsultaEstadoInfoEnviadaAlSiiConCanalResponse_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaEstadoInfoEnviadaAlSiiConCanalResponse");
    private final static QName _IngresarTransaccionesConCanalResponse_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "ingresarTransaccionesConCanalResponse");
    private final static QName _ConsultaEstadoInfoEnviadaAlSiiConCanal_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaEstadoInfoEnviadaAlSiiConCanal");
    private final static QName _ConsultaTransaccionesPorIdEnvioConCanalResponse_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaTransaccionesPorIdEnvioConCanalResponse");
    private final static QName _GetVersionResponse_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "getVersionResponse");
    private final static QName _ConsultaTransaccionesPorIdEnvioResponse_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "consultaTransaccionesPorIdEnvioResponse");
    private final static QName _IngresarTransaccionResponse_QNAME = new QName("http://ws.comprobanteboleta.oia.sdi.sii.cl", "ingresarTransaccionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.sii.sdi.oia.comprobanteboleta.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetVersion }
     * 
     */
    public GetVersion createGetVersion() {
        return new GetVersion();
    }

    /**
     * Create an instance of {@link ConsultaTransaccionesPorIdEnvioConCanal }
     * 
     */
    public ConsultaTransaccionesPorIdEnvioConCanal createConsultaTransaccionesPorIdEnvioConCanal() {
        return new ConsultaTransaccionesPorIdEnvioConCanal();
    }

    /**
     * Create an instance of {@link ConsultaEstadoInfoEnviadaAlSiiConCanalResponse }
     * 
     */
    public ConsultaEstadoInfoEnviadaAlSiiConCanalResponse createConsultaEstadoInfoEnviadaAlSiiConCanalResponse() {
        return new ConsultaEstadoInfoEnviadaAlSiiConCanalResponse();
    }

    /**
     * Create an instance of {@link IngresarTransaccionesConCanalResponse }
     * 
     */
    public IngresarTransaccionesConCanalResponse createIngresarTransaccionesConCanalResponse() {
        return new IngresarTransaccionesConCanalResponse();
    }

    /**
     * Create an instance of {@link ConsultaEstadoInfoEnviadaAlSiiConCanal }
     * 
     */
    public ConsultaEstadoInfoEnviadaAlSiiConCanal createConsultaEstadoInfoEnviadaAlSiiConCanal() {
        return new ConsultaEstadoInfoEnviadaAlSiiConCanal();
    }

    /**
     * Create an instance of {@link ConsultaTransaccionesPorIdEnvioConCanalResponse }
     * 
     */
    public ConsultaTransaccionesPorIdEnvioConCanalResponse createConsultaTransaccionesPorIdEnvioConCanalResponse() {
        return new ConsultaTransaccionesPorIdEnvioConCanalResponse();
    }

    /**
     * Create an instance of {@link ConsultaEstadoInfoPorTrackId }
     * 
     */
    public ConsultaEstadoInfoPorTrackId createConsultaEstadoInfoPorTrackId() {
        return new ConsultaEstadoInfoPorTrackId();
    }

    /**
     * Create an instance of {@link ConsultaTransaccionesPorIdEnvio }
     * 
     */
    public ConsultaTransaccionesPorIdEnvio createConsultaTransaccionesPorIdEnvio() {
        return new ConsultaTransaccionesPorIdEnvio();
    }

    /**
     * Create an instance of {@link GetVersionResponse }
     * 
     */
    public GetVersionResponse createGetVersionResponse() {
        return new GetVersionResponse();
    }

    /**
     * Create an instance of {@link IngresarTransaccionResponse }
     * 
     */
    public IngresarTransaccionResponse createIngresarTransaccionResponse() {
        return new IngresarTransaccionResponse();
    }

    /**
     * Create an instance of {@link ConsultaTransaccionesPorIdEnvioResponse }
     * 
     */
    public ConsultaTransaccionesPorIdEnvioResponse createConsultaTransaccionesPorIdEnvioResponse() {
        return new ConsultaTransaccionesPorIdEnvioResponse();
    }

    /**
     * Create an instance of {@link ConsultaEstadoInfoPorTrackIdConCanal }
     * 
     */
    public ConsultaEstadoInfoPorTrackIdConCanal createConsultaEstadoInfoPorTrackIdConCanal() {
        return new ConsultaEstadoInfoPorTrackIdConCanal();
    }

    /**
     * Create an instance of {@link IngresarTransaccion }
     * 
     */
    public IngresarTransaccion createIngresarTransaccion() {
        return new IngresarTransaccion();
    }

    /**
     * Create an instance of {@link ConsultaEstadoInfoPorTrackIdConCanalResponse }
     * 
     */
    public ConsultaEstadoInfoPorTrackIdConCanalResponse createConsultaEstadoInfoPorTrackIdConCanalResponse() {
        return new ConsultaEstadoInfoPorTrackIdConCanalResponse();
    }

    /**
     * Create an instance of {@link ConsultaEstadoInfoEnviadaAlSiiResponse }
     * 
     */
    public ConsultaEstadoInfoEnviadaAlSiiResponse createConsultaEstadoInfoEnviadaAlSiiResponse() {
        return new ConsultaEstadoInfoEnviadaAlSiiResponse();
    }

    /**
     * Create an instance of {@link IngresarTransaccionesConCanal }
     * 
     */
    public IngresarTransaccionesConCanal createIngresarTransaccionesConCanal() {
        return new IngresarTransaccionesConCanal();
    }

    /**
     * Create an instance of {@link ConsultaEstadoInfoEnviadaAlSii }
     * 
     */
    public ConsultaEstadoInfoEnviadaAlSii createConsultaEstadoInfoEnviadaAlSii() {
        return new ConsultaEstadoInfoEnviadaAlSii();
    }

    /**
     * Create an instance of {@link ConsultaEstadoInfoPorTrackIdResponse }
     * 
     */
    public ConsultaEstadoInfoPorTrackIdResponse createConsultaEstadoInfoPorTrackIdResponse() {
        return new ConsultaEstadoInfoPorTrackIdResponse();
    }

    /**
     * Create an instance of {@link TransaccionConCanalTo }
     * 
     */
    public TransaccionConCanalTo createTransaccionConCanalTo() {
        return new TransaccionConCanalTo();
    }

    /**
     * Create an instance of {@link RespuestaTo }
     * 
     */
    public RespuestaTo createRespuestaTo() {
        return new RespuestaTo();
    }

    /**
     * Create an instance of {@link TransaccionRespTo }
     * 
     */
    public TransaccionRespTo createTransaccionRespTo() {
        return new TransaccionRespTo();
    }

    /**
     * Create an instance of {@link TransaccionRespConCanalTo }
     * 
     */
    public TransaccionRespConCanalTo createTransaccionRespConCanalTo() {
        return new TransaccionRespConCanalTo();
    }

    /**
     * Create an instance of {@link RespuestaTransaccionesTo }
     * 
     */
    public RespuestaTransaccionesTo createRespuestaTransaccionesTo() {
        return new RespuestaTransaccionesTo();
    }

    /**
     * Create an instance of {@link TransaccionTo }
     * 
     */
    public TransaccionTo createTransaccionTo() {
        return new TransaccionTo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaEstadoInfoPorTrackIdConCanal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaEstadoInfoPorTrackIdConCanal")
    public JAXBElement<ConsultaEstadoInfoPorTrackIdConCanal> createConsultaEstadoInfoPorTrackIdConCanal(ConsultaEstadoInfoPorTrackIdConCanal value) {
        return new JAXBElement<ConsultaEstadoInfoPorTrackIdConCanal>(_ConsultaEstadoInfoPorTrackIdConCanal_QNAME, ConsultaEstadoInfoPorTrackIdConCanal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarTransaccion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "ingresarTransaccion")
    public JAXBElement<IngresarTransaccion> createIngresarTransaccion(IngresarTransaccion value) {
        return new JAXBElement<IngresarTransaccion>(_IngresarTransaccion_QNAME, IngresarTransaccion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaEstadoInfoPorTrackIdConCanalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaEstadoInfoPorTrackIdConCanalResponse")
    public JAXBElement<ConsultaEstadoInfoPorTrackIdConCanalResponse> createConsultaEstadoInfoPorTrackIdConCanalResponse(ConsultaEstadoInfoPorTrackIdConCanalResponse value) {
        return new JAXBElement<ConsultaEstadoInfoPorTrackIdConCanalResponse>(_ConsultaEstadoInfoPorTrackIdConCanalResponse_QNAME, ConsultaEstadoInfoPorTrackIdConCanalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarTransaccionesConCanal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "ingresarTransaccionesConCanal")
    public JAXBElement<IngresarTransaccionesConCanal> createIngresarTransaccionesConCanal(IngresarTransaccionesConCanal value) {
        return new JAXBElement<IngresarTransaccionesConCanal>(_IngresarTransaccionesConCanal_QNAME, IngresarTransaccionesConCanal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaEstadoInfoEnviadaAlSiiResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaEstadoInfoEnviadaAlSiiResponse")
    public JAXBElement<ConsultaEstadoInfoEnviadaAlSiiResponse> createConsultaEstadoInfoEnviadaAlSiiResponse(ConsultaEstadoInfoEnviadaAlSiiResponse value) {
        return new JAXBElement<ConsultaEstadoInfoEnviadaAlSiiResponse>(_ConsultaEstadoInfoEnviadaAlSiiResponse_QNAME, ConsultaEstadoInfoEnviadaAlSiiResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaEstadoInfoEnviadaAlSii }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaEstadoInfoEnviadaAlSii")
    public JAXBElement<ConsultaEstadoInfoEnviadaAlSii> createConsultaEstadoInfoEnviadaAlSii(ConsultaEstadoInfoEnviadaAlSii value) {
        return new JAXBElement<ConsultaEstadoInfoEnviadaAlSii>(_ConsultaEstadoInfoEnviadaAlSii_QNAME, ConsultaEstadoInfoEnviadaAlSii.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaEstadoInfoPorTrackIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaEstadoInfoPorTrackIdResponse")
    public JAXBElement<ConsultaEstadoInfoPorTrackIdResponse> createConsultaEstadoInfoPorTrackIdResponse(ConsultaEstadoInfoPorTrackIdResponse value) {
        return new JAXBElement<ConsultaEstadoInfoPorTrackIdResponse>(_ConsultaEstadoInfoPorTrackIdResponse_QNAME, ConsultaEstadoInfoPorTrackIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaTransaccionesPorIdEnvioConCanal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaTransaccionesPorIdEnvioConCanal")
    public JAXBElement<ConsultaTransaccionesPorIdEnvioConCanal> createConsultaTransaccionesPorIdEnvioConCanal(ConsultaTransaccionesPorIdEnvioConCanal value) {
        return new JAXBElement<ConsultaTransaccionesPorIdEnvioConCanal>(_ConsultaTransaccionesPorIdEnvioConCanal_QNAME, ConsultaTransaccionesPorIdEnvioConCanal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVersion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "getVersion")
    public JAXBElement<GetVersion> createGetVersion(GetVersion value) {
        return new JAXBElement<GetVersion>(_GetVersion_QNAME, GetVersion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaEstadoInfoPorTrackId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaEstadoInfoPorTrackId")
    public JAXBElement<ConsultaEstadoInfoPorTrackId> createConsultaEstadoInfoPorTrackId(ConsultaEstadoInfoPorTrackId value) {
        return new JAXBElement<ConsultaEstadoInfoPorTrackId>(_ConsultaEstadoInfoPorTrackId_QNAME, ConsultaEstadoInfoPorTrackId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaTransaccionesPorIdEnvio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaTransaccionesPorIdEnvio")
    public JAXBElement<ConsultaTransaccionesPorIdEnvio> createConsultaTransaccionesPorIdEnvio(ConsultaTransaccionesPorIdEnvio value) {
        return new JAXBElement<ConsultaTransaccionesPorIdEnvio>(_ConsultaTransaccionesPorIdEnvio_QNAME, ConsultaTransaccionesPorIdEnvio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaEstadoInfoEnviadaAlSiiConCanalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaEstadoInfoEnviadaAlSiiConCanalResponse")
    public JAXBElement<ConsultaEstadoInfoEnviadaAlSiiConCanalResponse> createConsultaEstadoInfoEnviadaAlSiiConCanalResponse(ConsultaEstadoInfoEnviadaAlSiiConCanalResponse value) {
        return new JAXBElement<ConsultaEstadoInfoEnviadaAlSiiConCanalResponse>(_ConsultaEstadoInfoEnviadaAlSiiConCanalResponse_QNAME, ConsultaEstadoInfoEnviadaAlSiiConCanalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarTransaccionesConCanalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "ingresarTransaccionesConCanalResponse")
    public JAXBElement<IngresarTransaccionesConCanalResponse> createIngresarTransaccionesConCanalResponse(IngresarTransaccionesConCanalResponse value) {
        return new JAXBElement<IngresarTransaccionesConCanalResponse>(_IngresarTransaccionesConCanalResponse_QNAME, IngresarTransaccionesConCanalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaEstadoInfoEnviadaAlSiiConCanal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaEstadoInfoEnviadaAlSiiConCanal")
    public JAXBElement<ConsultaEstadoInfoEnviadaAlSiiConCanal> createConsultaEstadoInfoEnviadaAlSiiConCanal(ConsultaEstadoInfoEnviadaAlSiiConCanal value) {
        return new JAXBElement<ConsultaEstadoInfoEnviadaAlSiiConCanal>(_ConsultaEstadoInfoEnviadaAlSiiConCanal_QNAME, ConsultaEstadoInfoEnviadaAlSiiConCanal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaTransaccionesPorIdEnvioConCanalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaTransaccionesPorIdEnvioConCanalResponse")
    public JAXBElement<ConsultaTransaccionesPorIdEnvioConCanalResponse> createConsultaTransaccionesPorIdEnvioConCanalResponse(ConsultaTransaccionesPorIdEnvioConCanalResponse value) {
        return new JAXBElement<ConsultaTransaccionesPorIdEnvioConCanalResponse>(_ConsultaTransaccionesPorIdEnvioConCanalResponse_QNAME, ConsultaTransaccionesPorIdEnvioConCanalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetVersionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "getVersionResponse")
    public JAXBElement<GetVersionResponse> createGetVersionResponse(GetVersionResponse value) {
        return new JAXBElement<GetVersionResponse>(_GetVersionResponse_QNAME, GetVersionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaTransaccionesPorIdEnvioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "consultaTransaccionesPorIdEnvioResponse")
    public JAXBElement<ConsultaTransaccionesPorIdEnvioResponse> createConsultaTransaccionesPorIdEnvioResponse(ConsultaTransaccionesPorIdEnvioResponse value) {
        return new JAXBElement<ConsultaTransaccionesPorIdEnvioResponse>(_ConsultaTransaccionesPorIdEnvioResponse_QNAME, ConsultaTransaccionesPorIdEnvioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarTransaccionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.comprobanteboleta.oia.sdi.sii.cl", name = "ingresarTransaccionResponse")
    public JAXBElement<IngresarTransaccionResponse> createIngresarTransaccionResponse(IngresarTransaccionResponse value) {
        return new JAXBElement<IngresarTransaccionResponse>(_IngresarTransaccionResponse_QNAME, IngresarTransaccionResponse.class, null, value);
    }

}
