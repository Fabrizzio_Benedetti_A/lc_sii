
package cl.sii.sdi.oia.comprobanteboleta.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para respuestaTo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="respuestaTo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ccbtTrackid" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codResp" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="descResp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaTo", propOrder = {
    "ccbtTrackid",
    "codResp",
    "descResp"
})
@XmlSeeAlso({
    TransaccionConCanalTo.class,
    RespuestaTransaccionesTo.class,
    TransaccionTo.class
})
public class RespuestaTo {

    protected Long ccbtTrackid;
    protected Integer codResp;
    protected String descResp;

    /**
     * Obtiene el valor de la propiedad ccbtTrackid.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCcbtTrackid() {
        return ccbtTrackid;
    }

    /**
     * Define el valor de la propiedad ccbtTrackid.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCcbtTrackid(Long value) {
        this.ccbtTrackid = value;
    }

    /**
     * Obtiene el valor de la propiedad codResp.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodResp() {
        return codResp;
    }

    /**
     * Define el valor de la propiedad codResp.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodResp(Integer value) {
        this.codResp = value;
    }

    /**
     * Obtiene el valor de la propiedad descResp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescResp() {
        return descResp;
    }

    /**
     * Define el valor de la propiedad descResp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescResp(String value) {
        this.descResp = value;
    }

}
