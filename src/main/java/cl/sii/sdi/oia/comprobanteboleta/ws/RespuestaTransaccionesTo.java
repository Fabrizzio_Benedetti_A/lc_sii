
package cl.sii.sdi.oia.comprobanteboleta.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para respuestaTransaccionesTo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="respuestaTransaccionesTo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.comprobanteboleta.oia.sdi.sii.cl}respuestaTo">
 *       &lt;sequence>
 *         &lt;element name="lineasError" type="{http://ws.comprobanteboleta.oia.sdi.sii.cl}TransaccionConCanalTo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tracksID" type="{http://www.w3.org/2001/XMLSchema}long" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaTransaccionesTo", propOrder = {
    "lineasError",
    "tracksID"
})
public class RespuestaTransaccionesTo
    extends RespuestaTo
{

    @XmlElement(nillable = true)
    protected List<TransaccionConCanalTo> lineasError;
    @XmlElement(nillable = true)
    protected List<Long> tracksID;

    /**
     * Gets the value of the lineasError property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineasError property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineasError().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransaccionConCanalTo }
     * 
     * 
     */
    public List<TransaccionConCanalTo> getLineasError() {
        if (lineasError == null) {
            lineasError = new ArrayList<TransaccionConCanalTo>();
        }
        return this.lineasError;
    }

    /**
     * Gets the value of the tracksID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tracksID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTracksID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getTracksID() {
        if (tracksID == null) {
            tracksID = new ArrayList<Long>();
        }
        return this.tracksID;
    }

	@Override
	public String toString() {
		return "RespuestaTransaccionesTo [lineasError=" + lineasError + ", tracksID=" + tracksID + ", ccbtTrackid="
				+ ccbtTrackid + ", codResp=" + codResp + ", descResp=" + descResp + ", getLineasError()="
				+ getLineasError() + ", getTracksID()=" + getTracksID() + ", getCcbtTrackid()=" + getCcbtTrackid()
				+ ", getCodResp()=" + getCodResp() + ", getDescResp()=" + getDescResp() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
