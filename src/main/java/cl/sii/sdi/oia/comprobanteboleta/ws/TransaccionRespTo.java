
package cl.sii.sdi.oia.comprobanteboleta.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para transaccionRespTo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="transaccionRespTo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.comprobanteboleta.oia.sdi.sii.cl}TransaccionTo">
 *       &lt;sequence>
 *         &lt;element name="cantidadTransacciones" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="fechaRecep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trackid" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transaccionRespTo", propOrder = {
    "cantidadTransacciones",
    "fechaRecep",
    "trackid"
})
public class TransaccionRespTo
    extends TransaccionTo
{

    protected Integer cantidadTransacciones;
    protected String fechaRecep;
    protected Long trackid;

    /**
     * Obtiene el valor de la propiedad cantidadTransacciones.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCantidadTransacciones() {
        return cantidadTransacciones;
    }

    /**
     * Define el valor de la propiedad cantidadTransacciones.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCantidadTransacciones(Integer value) {
        this.cantidadTransacciones = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaRecep.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaRecep() {
        return fechaRecep;
    }

    /**
     * Define el valor de la propiedad fechaRecep.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRecep(String value) {
        this.fechaRecep = value;
    }

    /**
     * Obtiene el valor de la propiedad trackid.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTrackid() {
        return trackid;
    }

    /**
     * Define el valor de la propiedad trackid.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTrackid(Long value) {
        this.trackid = value;
    }

}
