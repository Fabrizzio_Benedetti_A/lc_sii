
package cl.sii.sdi.oia.comprobanteboleta.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TransaccionTo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TransaccionTo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.comprobanteboleta.oia.sdi.sii.cl}respuestaTo">
 *       &lt;sequence>
 *         &lt;element name="rutInformante" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvInformante" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutContribuyente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dvContribuyente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaVenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalMontoNeto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalMontoExento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalMontoTotal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalMontoPropina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalMontoVuelto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalMontoDonacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalMontoTransaccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalValesEmitidos" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="identificadorEnvio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransaccionTo", propOrder = {
    "rutInformante",
    "dvInformante",
    "rutContribuyente",
    "dvContribuyente",
    "tipoDocumento",
    "fechaVenta",
    "totalMontoNeto",
    "totalMontoExento",
    "totalMontoTotal",
    "totalMontoPropina",
    "totalMontoVuelto",
    "totalMontoDonacion",
    "totalMontoTransaccion",
    "totalValesEmitidos",
    "identificadorEnvio"
})
@XmlSeeAlso({
    TransaccionRespTo.class
})
public class TransaccionTo
    extends RespuestaTo
{

    @XmlElement(required = true)
    protected String rutInformante;
    @XmlElement(required = true)
    protected String dvInformante;
    @XmlElement(required = true)
    protected String rutContribuyente;
    @XmlElement(required = true)
    protected String dvContribuyente;
    @XmlElement(required = true)
    protected String tipoDocumento;
    @XmlElement(required = true)
    protected String fechaVenta;
    @XmlElement(required = true)
    protected String totalMontoNeto;
    protected String totalMontoExento;
    @XmlElement(required = true)
    protected String totalMontoTotal;
    protected String totalMontoPropina;
    protected String totalMontoVuelto;
    protected String totalMontoDonacion;
    @XmlElement(required = true)
    protected String totalMontoTransaccion;
    @XmlElement(required = true)
    protected String totalValesEmitidos;
    @XmlElement(required = true)
    protected String identificadorEnvio;

    /**
     * Obtiene el valor de la propiedad rutInformante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutInformante() {
        return rutInformante;
    }

    /**
     * Define el valor de la propiedad rutInformante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutInformante(String value) {
        this.rutInformante = value;
    }

    /**
     * Obtiene el valor de la propiedad dvInformante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvInformante() {
        return dvInformante;
    }

    /**
     * Define el valor de la propiedad dvInformante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvInformante(String value) {
        this.dvInformante = value;
    }

    /**
     * Obtiene el valor de la propiedad rutContribuyente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutContribuyente() {
        return rutContribuyente;
    }

    /**
     * Define el valor de la propiedad rutContribuyente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutContribuyente(String value) {
        this.rutContribuyente = value;
    }

    /**
     * Obtiene el valor de la propiedad dvContribuyente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDvContribuyente() {
        return dvContribuyente;
    }

    /**
     * Define el valor de la propiedad dvContribuyente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDvContribuyente(String value) {
        this.dvContribuyente = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Define el valor de la propiedad tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaVenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaVenta() {
        return fechaVenta;
    }

    /**
     * Define el valor de la propiedad fechaVenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaVenta(String value) {
        this.fechaVenta = value;
    }

    /**
     * Obtiene el valor de la propiedad totalMontoNeto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalMontoNeto() {
        return totalMontoNeto;
    }

    /**
     * Define el valor de la propiedad totalMontoNeto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalMontoNeto(String value) {
        this.totalMontoNeto = value;
    }

    /**
     * Obtiene el valor de la propiedad totalMontoExento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalMontoExento() {
        return totalMontoExento;
    }

    /**
     * Define el valor de la propiedad totalMontoExento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalMontoExento(String value) {
        this.totalMontoExento = value;
    }

    /**
     * Obtiene el valor de la propiedad totalMontoTotal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalMontoTotal() {
        return totalMontoTotal;
    }

    /**
     * Define el valor de la propiedad totalMontoTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalMontoTotal(String value) {
        this.totalMontoTotal = value;
    }

    /**
     * Obtiene el valor de la propiedad totalMontoPropina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalMontoPropina() {
        return totalMontoPropina;
    }

    /**
     * Define el valor de la propiedad totalMontoPropina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalMontoPropina(String value) {
        this.totalMontoPropina = value;
    }

    /**
     * Obtiene el valor de la propiedad totalMontoVuelto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalMontoVuelto() {
        return totalMontoVuelto;
    }

    /**
     * Define el valor de la propiedad totalMontoVuelto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalMontoVuelto(String value) {
        this.totalMontoVuelto = value;
    }

    /**
     * Obtiene el valor de la propiedad totalMontoDonacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalMontoDonacion() {
        return totalMontoDonacion;
    }

    /**
     * Define el valor de la propiedad totalMontoDonacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalMontoDonacion(String value) {
        this.totalMontoDonacion = value;
    }

    /**
     * Obtiene el valor de la propiedad totalMontoTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalMontoTransaccion() {
        return totalMontoTransaccion;
    }

    /**
     * Define el valor de la propiedad totalMontoTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalMontoTransaccion(String value) {
        this.totalMontoTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad totalValesEmitidos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalValesEmitidos() {
        return totalValesEmitidos;
    }

    /**
     * Define el valor de la propiedad totalValesEmitidos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalValesEmitidos(String value) {
        this.totalValesEmitidos = value;
    }

    /**
     * Obtiene el valor de la propiedad identificadorEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificadorEnvio() {
        return identificadorEnvio;
    }

    /**
     * Define el valor de la propiedad identificadorEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificadorEnvio(String value) {
        this.identificadorEnvio = value;
    }

}
